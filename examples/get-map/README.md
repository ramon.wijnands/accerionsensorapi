# Script to retrieve the map from sensor (Triton, Jupiter)
**Prerequisites** <br>
Build the API first.

**Compile example code** <br>
Make sure to change the ip address within 'main.cpp' to match your sensor. Then follow the steps below: 

<code> $ cd get-map </code> <br>
<code> $ mkdir build </code> <br>
<code> $ cd build </code> <br>
<code> $ cmake .. </code> <br>
<code> $ make </code> <br>

**Syntax** <br>
<code> $ ./getMap 192.168.178.100 "map_directory" </code> <br>

**Example**  <br>
<code> $ demykremers@demykremers-HP-ProBook-450-G6:~/codes/accerionsensorapi/examples/get-map/build$ ./getMap 192.168.0.10 "./" </code> <br>


**Response** <br>
Looking for updateService.. <br>
Sensor with serial number found: 598100173 <br>
Found sensor! <br>
Trying to retrieve map: getMapViaAPI598100173-05-11-2020-13-28.amf <br>
FileSenderStatus: 0 <br>
RECEIVED MAP INFO FROM SENSOR: 598100173 <br>
RECEIVED A MAP PACKAGE FROM SENSOR: 598100173 <br>
FileSenderStatus: 15 <br>
Progress: 100 <br>
MAP DONE FOR: 598100173 <br>
Map transfer successful <br>

**Results** <br>
598100173-05-11-2020-13-31.amf in current directory on local machine (pc/NUC)
