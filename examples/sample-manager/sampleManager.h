#include <AccerionSensorAPI/AccerionSensorAPI.h>

class SampleManager
{
 public:
 SampleManager(int first, int second, int third, int fourth);
 ~SampleManager();
 void toggleMapping(bool on, uint16_t clusterID);
 void clearClusterLibrary();
 void setSensorPose(Pose poseStruct);
 void toggleAbsoluteMode(bool on);

 private:
 void absCB(Acknowledgement ack);
 void poseCB(Pose sp);
 void clearCLCB(Acknowledgement ack);
 void mappingCB(Acknowledgement ack);
 AccerionSensorManager* sensorManager;
 AccerionSensor* mySensor;
 uint16_t currentClusterID_;
 Pose pose_;
};