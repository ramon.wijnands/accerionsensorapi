/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionUpdateService.h"

AccerionUpdateService::AccerionUpdateService(Address ip, std::string serial, Address localIP)
{
    /*Initialize CRC Checking*/
    crc8_.crcInit();
    char buf[3 * 4 + 3 * 1 + 1];
    snprintf(buf, sizeof(buf), "%d.%d.%d.%d", ip.first, ip.second, ip.third, ip.fourth);
    // store remote IP address in remote:
    struct sockaddr_in remote;
    inet_pton(AF_INET, buf, &(remote.sin_addr));

    // store local IP address in local:
    localIP_ = localIP;

    sensorSerialNumber_ = stoi(serial);

    tcpClient = new TCPClient(remote.sin_addr, UPDATE_SERVICE_PORT_TCP);
    tcpClient->sensorSerialNumber_ = stoi(serial);
    std::thread tcpThread(&AccerionUpdateService::runTCPCommunication, this);
    tcpThread.detach();
}

AccerionUpdateService::~AccerionUpdateService()
{
    delete udpReceiver;
}

void AccerionUpdateService::runTCPCommunication()
{
    std::vector<Command> incomingCommandsTotal_;
    std::vector<Command> outgoingCommandsTotal_;
    std::vector<uint8_t> receivedMSG_;

    tcpClient->connectToServer();
    while (runTCP)
    {
        //receive
        while (tcpClient->receiveMessage())
        {
            if (!lastMessageWasBroken_)
            {
                receivedMSG_.clear();
            }
            receivedMSG_.insert(receivedMSG_.end(), tcpClient->getReceivedMessage(), tcpClient->getReceivedMessage() + tcpClient->getReceivedNumOfBytes());
            receivedCommand_.clear();
            parseMessage(incomingCommandsTotal_, receivedMSG_);
        }
        
        readMessages(incomingCommandsTotal_, outgoingCommandsTotal_);

        incomingCommandsTotal_.clear();

        //send..
        if (outgoingCommandsMutex.try_lock())
        {
            tcpClient->sendMessages(outgoingCommands);
            clearOutgoingCommands();
            outgoingCommandsMutex.unlock();
        }
        outgoingCommandsTotal_.clear();
    }
}

void AccerionUpdateService::readMessages(std::vector<Command> &incomingCommands, std::vector<Command> &outgoingCommands)
{
    auto it = incomingCommands.begin();
    while (it != incomingCommands.end())
    {
        receivedCommandID_ = (*it).commandID_;
        receivedCommand_ = (*it).command_;

        /*Check command, set modes according to command*/
        switch (receivedCommandID_)
        {
        case CommandIDs::PRD_HEARTBEAT_INFO:
            outputHeartBeat(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_DIAGNOSTICS:
            outputDiagnostics(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_LOGS:
            retrievedLogPiece(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_PLACE_CALIB:
            retrievedCalibAck(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_UPDATE:
            retrievedUpdateAck(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        default:
            it = incomingCommands.erase(it);
            break;
        }
    }
}

void AccerionUpdateService::parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_)
{
    uint8_t receivedSerialNumberData[4];

    //uint8_t receivedTotalBytesInMessageData[4];
    uint32_t receivedTotalBytesInMessage;
    uint32_t numberOfBytesLeft = receivedMessage_.size();
    uint32_t numberOfReadBytes = 0;

    if (debugMode_)
    {
        std::cout << "[AccerionUpdateService::parseMessages] numberOfBytesLeft is " << numberOfBytesLeft << std::endl;
        std::cout << "[AccerionUpdateService::parseMessages] receivedMessage_. Length is " << receivedMessage_.size() << std::endl;
    }

    while (numberOfBytesLeft > 0)
    {
        receivedCommand_.clear();

        if (numberOfBytesLeft >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
        {
            for (unsigned int i = 0; i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i++)
            {
                /*Get Serial Number*/
                if (i >= CommandFields::SERIAL_NUMBER_BEGIN_BYTE && i < CommandFields::SERIAL_NUMBER_BEGIN_BYTE + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH)
                {
                    receivedSerialNumberData[i] = receivedMessage_[i + numberOfReadBytes];
                }
                /*Get Command Number*/
                else if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE && i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
                {
                    receivedCommandID_ = receivedMessage_[i + numberOfReadBytes];
                    if (debugMode_)
                        std::cout << "[AccerionUpdateService::parseMessages] Received command: " << std::hex << +receivedCommandID_ << std::dec << std::endl;
                }
            }
            // Extract number of bytes in message
            if (receivedCommandID_ != CMD_REPLACE_CLUSTER_G2O && receivedCommandID_ != CMD_PLACE_MAP && receivedCommandID_ != ACK_CONSOLE_OUTPUT_INFO && receivedCommandID_ != ACK_LOGS)
            {
                auto iter = commandValues.find(receivedCommandID_);
                if (iter != commandValues.end())
                {
                    receivedTotalBytesInMessage = std::get<1>(iter->second); //std::get<1>(commandValues.at(receivedCommandID_));
                }
                else
                {
                    std::cout << "Unknown Command received" << std::endl;
                    receivedTotalBytesInMessage = 0;
                }
            }
            else
            {
                uint8_t receivedNumOfBytesData[4];
                receivedNumOfBytesData[0] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes];
                receivedNumOfBytesData[1] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 1];
                receivedNumOfBytesData[2] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 2];
                receivedNumOfBytesData[3] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 3];

                receivedTotalBytesInMessage = ntohl(*((uint32_t *)&receivedNumOfBytesData));
                if (debugMode_)
                    std::cout << "[AccerionUpdateService::parseMessages] Received length: " << receivedTotalBytesInMessage << std::endl;
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            if (debugMode_)
            {
                std::cout << "[AccerionUpdateService::parseMessages] Received message was broken 1" << std::endl;
            }
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }

        if (receivedTotalBytesInMessage <= numberOfBytesLeft)
        {
            for (unsigned int i = CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i < receivedTotalBytesInMessage; i++)
            {
                /*Get Command Data*/
                if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH && i < receivedTotalBytesInMessage - 1)
                {
                    receivedCommand_.push_back(receivedMessage_[i + numberOfReadBytes]);
                }
                /*Get CRC8 Data*/
                else
                {
                    receivedCRC8_ = receivedMessage_[i + numberOfReadBytes];
                }
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            if (debugMode_)
            {
                std::cout << "[AccerionUpdateService::parseMessages] Received message was broken 2 with no of bytesleft: " << numberOfBytesLeft << std::endl;
            }
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            if (debugMode_)
            {
                std::cout << "[AccerionUpdateService::parseMessages]receivedMessage_. Length after broken 2 is " << receivedMessage_.size() << std::endl;
            }
            break;
        }
        lastMessageWasBroken_ = false;

        /*Check serial number*/
        receivedSerialNumber_ = ntohl(*((uint32_t *)&receivedSerialNumberData));

        uint8_t calculatedCRC8 = crc8_.crcFast((uint8_t *)receivedMessage_.data() + numberOfReadBytes, receivedTotalBytesInMessage - 1);
        if (receivedSerialNumber_ != sensorSerialNumber_ && receivedSerialNumber_ != DEFAULT_SERIAL_NUMBER && sensorSerialNumber_ != DEFAULT_SERIAL_NUMBER)
        {
            if (debugMode_)
                std::cout << "Received Message is not for this Jupiter, which has serial number := " << std::hex << sensorSerialNumber_ << " it is intended for serial number := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            messageReady_ = false;
            return;
        }
        if (receivedCRC8_ != calculatedCRC8)
        {
            if (debugMode_)
                std::cout << "Received Message does not have the correct crc8 code, it has the wrong code := " << std::hex << +receivedCRC8_ << std::dec << std::endl;
            messageReady_ = false;
            return;
        }

        if (debugMode_)
            std::cout << "Command length of the to be inserted command: " << receivedCommand_.size() << std::endl;

        commands.emplace_back(receivedCommandID_, receivedCommand_);

        if (debugMode_)
        {
            std::cout << "Received serialNumber is := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            std::cout << "Received command number is := " << std::hex << +receivedCommandID_ << std::dec << std::endl;
            std::cout << "Received command size is := " << receivedCommand_.size() << std::endl;
        }

        numberOfReadBytes += receivedTotalBytesInMessage;
        numberOfBytesLeft -= receivedTotalBytesInMessage;
        receivedCommand_.clear();
    }
}

void AccerionUpdateService::subscribeToHeartBeat(_heartBeatCallBack hbCallback)
{
    heartBeatCallBack = hbCallback;
}

void AccerionUpdateService::outputHeartBeat(std::vector<uint8_t> data)
{
    HeartBeat hb;
    hb.ipAddrFirst = data[0];
    hb.ipAddrSecond = data[1];
    hb.ipAddrThird = data[2];
    hb.ipAddrFourth = data[3];
    hb.uniFirst = data[4];
    hb.uniSecond = data[5];
    hb.uniThird = data[6];
    hb.uniFourth = data[7];

    if (heartBeatCallBack)
    {
        heartBeatCallBack(hb);
    }
}

void AccerionUpdateService::subscribeToDiagnostics(_diagnosticsCallBack diagCallback)
{
    diagnosticsCallBack = diagCallback;
}

void AccerionUpdateService::outputDiagnostics(std::vector<uint8_t> data)
{
    Diagnostics diag;

    diag.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&data[0])) / 1000000.0;
    diag.modes = (uint16_t)ntohs(*((uint16_t *)&data[8]));
    diag.warningCodes = (uint16_t)ntohs(*((uint16_t *)&data[10]));
    diag.errorCodes = (uint32_t)ntohl(*((uint32_t *)&data[12]));
    diag.statusCodes = data[16];

    if (diagnosticsCallBack)
    {
        diagnosticsCallBack(diag);
    }
}

SerialNumber AccerionUpdateService::getSerialNumber()
{
    SerialNumber sn;
    sn.serialNumber = sensorSerialNumber_;
    return sn;
}

bool AccerionUpdateService::getLogs(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    if (!logsVars_.isInProgress)
    {
        commandIDToBeSent_ = CMD_GET_LOGS;
        logsVars_.isInProgress = true;
        logsVars_.totalMessagesToBeTransferred = 0;
        logsVars_.msgcounter = 0;
        progressCallBack = progressCB;
        doneCallBack = doneCB;
        statusCallBack = statusCB;
        logsVars_.filePath = destinationPath;
        return retrieveFirstLogPiece();
    }
    else
    {
        statusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionUpdateService::getBackupLogs(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    if (!logsVars_.isInProgress)
    {
        commandIDToBeSent_ = CMD_GET_BACKUP_LOGS;
        logsVars_.isInProgress = true;
        logsVars_.totalMessagesToBeTransferred = 0;
        logsVars_.msgcounter = 0;
        progressCallBack = progressCB;
        doneCallBack = doneCB;
        statusCallBack = statusCB;
        logsVars_.filePath = destinationPath;
        return retrieveFirstLogPiece();
    }
    else
    {
        statusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionUpdateService::retrieveFirstLogPiece()
{
    statusCallBack(PACKING_LOGS);

    if (!tcpClient->getConnected())
    {
        statusCallBack(CONNECTION_FAILED);
        logsVars_.filesSuccessfullyTransferred = false;
        logsVars_.isInProgress = false;
        return false;
    }

    if (FileExists(logsVars_.filePath))
    {
        if (remove(logsVars_.filePath.c_str()) != 0)
        {
            statusCallBack(FAILED_TO_REMOVE_EXISTING);
            logsVars_.filesSuccessfullyTransferred = false;
            logsVars_.isInProgress = false;
            return false;
        }
    }

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(commandIDToBeSent_, UINT32Command(commandIDToBeSent_, 0).serialize());
    outgoingCommandsMutex.unlock();
    return true;
}

void AccerionUpdateService::retrievedLogPiece(std::vector<uint8_t> receivedCommand_)
{
    //quint8 cmdType = *(reinterpret_cast<quint8*>(&receivedCommand_[4]));
    //uint32_t msgLength  = (uint32_t) ntohl(*((uint32_t*)&receivedCommand_[0]));

    if (receivedCommand_[4] == 0x00) // done, check next
    {
        logsVars_.isInProgress = false;
        fclose(logsVars_.file);
        doneCallBack(logsVars_.filesSuccessfullyTransferred,"Success");
    }
    else if (receivedCommand_[4] == 0x01) // fail, check next
    {
        logsVars_.isInProgress = false;
        if (logsVars_.totalMessagesToBeTransferred != 0)
        {
            fclose(logsVars_.file);
        }
        logsVars_.filesSuccessfullyTransferred = false;
        doneCallBack(logsVars_.filesSuccessfullyTransferred,"Failed");
    }
    else if (receivedCommand_[4] == 0x02) // info received
    {
        logsVars_.totalMessagesToBeTransferred = (uint32_t)ntohl(*((uint32_t *)&receivedCommand_[5]));
        logsVars_.file = fopen(logsVars_.filePath.c_str(), "ab");
        if (logsVars_.file)
        {
            logsVars_.msgcounter++;
        }
        retrieveNextLogPiece();
    }
    else if (receivedCommand_[4] == 0x03) // pckg received
    {
        double prgrss = ((logsVars_.msgcounter)*100.0 / logsVars_.totalMessagesToBeTransferred);
        statusCallBack(RETRIEVING_LOGS);
        progressCallBack(static_cast<int>(prgrss));

        size_t bytesRead = receivedCommand_.size() - 5;

        fwrite(&receivedCommand_[5], sizeof(unsigned char), bytesRead, logsVars_.file);

        fflush(logsVars_.file);

        logsVars_.msgcounter++;
        retrieveNextLogPiece();
    }
}

void AccerionUpdateService::retrieveNextLogPiece()
{
    if (!tcpClient->getConnected())
    {
        statusCallBack(CONNECTION_FAILED);
        logsVars_.filesSuccessfullyTransferred = false;
        logsVars_.isInProgress = false;
        return;
    }

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(commandIDToBeSent_, UINT32Command(commandIDToBeSent_, logsVars_.msgcounter).serialize());
    outgoingCommandsMutex.unlock();
}

bool AccerionUpdateService::sendCalibration(std::string sourcePath, _doneCallBack doneCB, std::string key)
{
    if(!doneCB)
    {
        return false;
    }

    if (!tcpClient->getConnected())
    {
        return false;
    }

    std::ifstream in(sourcePath, std::ios::ate);
    in.seekg(0, std::ios::end);
    calibVars_.totalFileSize = static_cast<int>(in.tellg());

    // Check if object is valid
    if (!in)
    {
        return false;
    }

    if(key.size() != 16)
    {
        return false;
    }

    in.close();

    calibDoneCallBack = doneCB;


    //sending first packet message
    calibVars_.file = fopen(sourcePath.c_str(), "rb");
    if (tcpClient->getConnected())
    {
        if (calibVars_.file)
        {
            size_t bytesRead = 0;
            unsigned char buffer[USBufferSize]; // array of bytes, not pointers-to-bytes
            // read up to sizeof(buffer) bytes
            bytesRead = fread(buffer, 1, sizeof(buffer), calibVars_.file);

            // process bytesRead worth of data in buffer
            std::vector<uint8_t> dataToSend;
            for (int i = 0; i < static_cast<int>(bytesRead); i++) // send package per buffer size
            {
                dataToSend.emplace_back(buffer[i]);
            }
            std::cout << "bytesRead: " << bytesRead << std::endl;
            outgoingCommandsMutex.lock();
            outgoingCommands.emplace_back(CMD_PLACE_CALIB, CalibrationFileCommand(CMD_PLACE_CALIB, dataToSend, key).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
            outgoingCommandsMutex.unlock();

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void AccerionUpdateService::retrievedCalibAck(std::vector<uint8_t> receivedCommand_)
{
    bool result = false;
    if (receivedCommand_[0] == 0x01)
    {
        result = true;
    }
    else if (receivedCommand_[0] == 0x02)
    {
        result = false;
    }
    std::cout << "Calib callback: " << result << std::endl;
    if(calibDoneCallBack)
    {
        calibDoneCallBack(result,"");
    }
}

bool AccerionUpdateService::updateSensor(std::string sourcePath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    if (!updateVars_.isInProgress)
    {
        updateVars_.totalMessagesToBeTransferred = 0;
        updateVars_.msgcounter = 0;
        progressCallBack = progressCB;
        doneCallBack = doneCB;
        statusCallBack = statusCB;
        updateVars_.filePath = sourcePath;
        return sendFirstUpdatePiece();
    }
    else
    {
        statusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionUpdateService::sendFirstUpdatePiece()
{
    updateVars_.msgcounter = 0;
    updateVars_.totalsent = 0;
    updateVars_.totalFileSize = 0;
    updateVars_.totalMessagesToBeTransferred = 0;

    if (!tcpClient->getConnected())
    {
        statusCallBack(CONNECTION_FAILED);
        updateVars_.filesSuccessfullyTransferred = false;
        updateVars_.isInProgress = false;
        return false;
    }

    std::ifstream in(updateVars_.filePath, std::ios::ate);
    in.seekg(0, std::ios::end);
    updateVars_.totalFileSize = static_cast<long long>(in.tellg());
    int fileSizeInMB = updateVars_.totalFileSize / 1024 / 1024;
    
    // Check if object is valid
    if (!in)
    {
        std::cout << "File open failure..." << std::endl;
        updateVars_.filesSuccessfullyTransferred = false;
        updateVars_.isInProgress = false;
        statusCallBack(FAILED_TO_OPEN_FILE);
        return false;
    }

    in.close();
    updateVars_.totalMessagesToBeTransferred = static_cast<uint32_t>(updateVars_.totalFileSize / bufferSize);
    //std::cout << "(updateVars_.totalFileSize % bufferSize)" << updateVars_.totalFileSize % bufferSize << std::endl;
    if(updateVars_.totalFileSize % bufferSize != 0)
    {
        updateVars_.totalMessagesToBeTransferred += 1;
    }

    //Sending start message
    uint32_t messageLength = 0 + std::get<1>(commandValues.find(CMD_UPDATE)->second);
    //uint8_t option = 0;
    std::vector<uint8_t> empty;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_UPDATE, LegacyUploadCommand(CMD_UPDATE, messageLength, 0, fileSizeInMB, empty).serialize()); // first 0 is packettype = start, second is messageIndex, which is still 0 here.
    outgoingCommandsMutex.unlock();
    //socketsVector[transferCounter]->tcpSocket_.flush();

    //sending first packet message
    updateVars_.file = fopen(updateVars_.filePath.c_str(), "rb");
    if (tcpClient->getConnected())
    {
        if (updateVars_.file)
        {
            fseek(updateVars_.file, 0, SEEK_SET);
            size_t bytesRead = 0;
            unsigned char buffer[bufferSize]; // array of bytes, not pointers-to-bytes
            // read up to sizeof(buffer) bytes
            bytesRead = fread(buffer, 1, sizeof(buffer), updateVars_.file);

            // process bytesRead worth of data in buffer

            auto messageLength = static_cast<uint32_t>(bytesRead + std::get<1>(commandValues.find(CMD_UPDATE)->second));
            std::vector<uint8_t> dataToSend = std::vector<uint8_t>(buffer, buffer + bytesRead);
            outgoingCommandsMutex.lock();
            outgoingCommands.emplace_back(CMD_UPDATE, LegacyUploadCommand(CMD_UPDATE, messageLength, 1, 0, dataToSend).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
            outgoingCommandsMutex.unlock();

            updateVars_.totalsent += bytesRead;
            updateVars_.msgcounter++;
            statusCallBack(SENDING_UPDATE);
            std::cout << "Update piece sent.." << std::endl;
            return true;
        }
        else
        {
            std::cout << "File reading failure.." << std::endl;
            updateVars_.filesSuccessfullyTransferred = false;
            updateVars_.isInProgress = false;
            statusCallBack(FAILED_TO_OPEN_FILE);
            return false;
        }
    }
    else
    {
        std::cout << "Connetion failure...." << std::endl;
        statusCallBack(CONNECTION_FAILED);
        updateVars_.filesSuccessfullyTransferred = false;
        updateVars_.isInProgress = false;
        return false;
    }
}

void AccerionUpdateService::retrievedUpdateAck(std::vector<uint8_t> receivedCommand_) // 1 = done, 2 = failed, 3 = pckg success, 4 = package failed, 5 = encrypting, 6=  unzipping
{
    /*
    *    1: Update done
    *    2: Update failed
    *    3: Package Received Succesfulyy
    *    4: Package Failed
    *    5: Encrypting
    *    6: Unzipping
    *   11: Retrieval failed
    *   12: Decryption failed
    *   13: Stopping failed
    *   14: Backup failed
    *   15: Unzip failed
    *   16: Start failed
    *   17: Restore backup failed
    *   18: Script execution failed
    *   19: Script deletion failed
    *   20: Unknown error in script execution
    * */
    if (receivedCommand_[0] == 0x01) // update succeeded, check next
    {
        updateVars_.isInProgress = false;
        if(updateVars_.file != NULL)
        {
            fclose(updateVars_.file);
            updateVars_.file = NULL;
        }
        doneCallBack(updateVars_.filesSuccessfullyTransferred, "Success");
    }
    else if (receivedCommand_[0] == 0x02) // update failed, check next
    {
        updateVars_.filesSuccessfullyTransferred = false;
        updateVars_.isInProgress = false;
        if(updateVars_.file != NULL)
        {
            fclose(updateVars_.file);
            updateVars_.file = NULL;
        }
        doneCallBack(updateVars_.filesSuccessfullyTransferred, "Failure");
    }
    else if (receivedCommand_[0] == 0x03 || receivedCommand_[0] == 0x04) // either send next message, current message or stop message
    {
        double prgrss = ((updateVars_.msgcounter)*100.0 / updateVars_.totalMessagesToBeTransferred);
        statusCallBack(SENDING_UPDATE);
        progressCallBack(static_cast<int>(prgrss));
        //int msgReceived = (uint32_t) ntohl(*((uint32_t*)&receivedCommand_[1]));

        if (prgrss == 100.0)
        {
            statusCallBack(UNPACKING_AND_UPDATING);
        }

        if (static_cast<int>(updateVars_.msgcounter) == updateVars_.totalMessagesToBeTransferred && updateVars_.totalsent == updateVars_.totalFileSize) // everything has been sent, so sending stop message
        {
            uint32_t messageLength = static_cast<uint32_t>(0 + std::get<1>(commandValues.find(CMD_UPDATE)->second));
            uint8_t option = 2; // 2 = normal stop, which will replace
            std::vector<uint8_t> empty;
            if (tcpClient->getConnected())
            {
                outgoingCommandsMutex.lock();
                outgoingCommands.emplace_back(CMD_UPDATE, LegacyUploadCommand(CMD_UPDATE, messageLength, option, 0, empty).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
                outgoingCommandsMutex.unlock();
            }
            else
            {
                updateVars_.isInProgress = false;
                updateVars_.filesSuccessfullyTransferred = false;
                statusCallBack(CONNECTION_FAILED);
                return;
            }
        }
        else
        {
            //send next message..
            if (updateVars_.file)
            {
                unsigned char buffer[bufferSize]; // array of bytes, not pointers-to-bytes
                int bytesRead = fread(buffer, 1, sizeof(buffer), updateVars_.file);
                auto messageLength = static_cast<uint32_t>(bytesRead + std::get<1>(commandValues.find(CMD_UPDATE)->second));
                std::vector<uint8_t> dataToSend = std::vector<uint8_t>(buffer, buffer + bytesRead);
                outgoingCommandsMutex.lock();
                outgoingCommands.emplace_back(CMD_UPDATE, LegacyUploadCommand(CMD_UPDATE, messageLength, 1, updateVars_.msgcounter, dataToSend).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
                outgoingCommandsMutex.unlock();
                updateVars_.totalsent += bytesRead;
                updateVars_.msgcounter++;
                std::cout << "Messagecounter: " << updateVars_.msgcounter << std::endl;
            }
        }
    }
    else if (receivedCommand_[0] == 0x05 || receivedCommand_[0] == 0x06)
    {
        statusCallBack(UNPACKING_AND_UPDATING);
    }
    else
    {   
        updateVars_.filesSuccessfullyTransferred = false;
        updateVars_.isInProgress = false;
        if(updateVars_.file != NULL)
        {
            fclose(updateVars_.file);
            updateVars_.file = NULL;
        }
        switch(receivedCommand_[0])
        {
            case 0x13:
                doneCallBack(updateVars_.filesSuccessfullyTransferred, "Could not stop sensor");
            break;
            case 0x14:
                doneCallBack(updateVars_.filesSuccessfullyTransferred, "Could not backup sensor");
            break;
            case 0x15:
                doneCallBack(updateVars_.filesSuccessfullyTransferred, "Could not unpack update");
            break;
            case 0x16:
                doneCallBack(updateVars_.filesSuccessfullyTransferred, "Could not start sensor");
            break;
            case 0x17:
                doneCallBack(updateVars_.filesSuccessfullyTransferred, "Could not restore backup");
            break;
            case 0x18:
                doneCallBack(updateVars_.filesSuccessfullyTransferred, "Errorcode 18");
            break;
            case 0x19:
                doneCallBack(updateVars_.filesSuccessfullyTransferred, "Errorcode 19");
            break;
            case 0x20:
                doneCallBack(updateVars_.filesSuccessfullyTransferred, "Errorcode 20");
            break;
        }

    }
}