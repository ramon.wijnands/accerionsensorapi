/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensor.h"

AccerionSensor::AccerionSensor(Address ip, const std::string& serial, Address localIP, ConnectionType conType)
{
#ifdef DEBUG
    debugMode_ = true;
#endif
    /*Initialize CRC Checking*/
    crc8_.crcInit();
    char buf[3 * 4 + 3 * 1 + 1];
    snprintf(buf, sizeof(buf), "%d.%d.%d.%d", ip.first, ip.second, ip.third, ip.fourth);
    // store remote IP address in remote:
    struct sockaddr_in remote;

#ifdef _WIN32 
    InetPton(AF_INET, buf, &(remote.sin_addr));
#else
    inet_pton(AF_INET, buf, &(remote.sin_addr));
#endif

    // store local IP address in local:
    localIP_ = localIP;

    sensorSerialNumber_ = stoi(serial);
    connectionType = conType;

    udpReceiver = new UDPReceiver(API_RECEIVE_PORT_UDP);
    udpTransmitter = new UDPTransmitter(JUPITER_RECEIVE_PORT_UDP);
    udpTransmitter->setIPAddress(remote.sin_addr);
    udpTransmitter->sensorSerialNumber_ = stoi(serial);
    udpThread = std::thread(&AccerionSensor::runUDPCommunication, this);

    tcpClient = new TCPClient(remote.sin_addr, TCP_PORT);
    tcpClient->sensorSerialNumber_ = stoi(serial);
    tcpThread = std::thread(&AccerionSensor::runTCPCommunication, this);
}

AccerionSensor::~AccerionSensor()
{
    runUDP = false;
    runTCP = false;
    if(udpThread.joinable()) udpThread.join();
    if(tcpThread.joinable()) tcpThread.join();
    delete udpTransmitter;
    delete udpReceiver;
}

void AccerionSensor::runUDPCommunication()
{
    APIProfileTimer profileTimer("AccerionSensor UDP thread", true);
    float maxDuration = 1e6/NetworkConstants::APIThroughput;

    std::vector<Command> incomingCommandsTotal_;
    std::vector<Command> outgoingCommandsTotal_;
    std::vector<uint8_t> receivedMSG_;

    EnabledMessageType udpMsgType = EnabledMessageType::BOTH;
    UDPStrategy udpStrat = UDPStrategy::BROADCAST;

    switch (connectionType)
    {
    case ConnectionType::CONNECTION_TCP:
        udpStrat = UDPStrategy::UNTOUCHED;
        break;
    case ConnectionType::CONNECTION_UDP_BROADCAST:
        udpMsgType = EnabledMessageType::BOTH;
        udpStrat = UDPStrategy::BROADCAST;
        break;
    case ConnectionType::CONNECTION_UDP_UNICAST:
        udpMsgType = EnabledMessageType::BOTH;
        udpStrat = UDPStrategy::UNICAST;
        break;
    case ConnectionType::CONNECTION_TCP_DISABLE_UDP:
        udpMsgType  = EnabledMessageType::INACTIVE;
        udpStrat    = UDPStrategy::BROADCAST;
        break;
    // case ConnectionType::CONNECTION_UDP_UNICAST_NO_HB:
    //     udpMsgType  = EnabledMessageType::BOTH;
    //     udpStrat = UDPStrategy::UNICAST_NO_HB;
    //     break;
    }

    if (udpStrat != UDPStrategy::UNTOUCHED)
    {
        outgoingCommandsMutex.lock();
        outgoingCommands.emplace_back(CMD_SET_UDP_SETTINGS,
                                      UDPSettingsCommand(CMD_SET_UDP_SETTINGS, localIP_.first, localIP_.second,
                                                         localIP_.third, localIP_.fourth, udpMsgType,
                                                         udpStrat).serialize());
        outgoingCommandsMutex.unlock();
    }

    while (runUDP)
    {
        profileTimer.startLoopTime();
        //receive
        while (udpReceiver->ReceiveMessage())
        {
            receivedMSG_.clear();
            receivedMSG_.insert(receivedMSG_.end(), udpReceiver->getReceivedMessage(), udpReceiver->getReceivedMessage() + udpReceiver->getReceivedNumOfBytes());
            receivedCommand_.clear();
            parseMessage(incomingCommandsTotal_, receivedMSG_);
        }

        readMessages(incomingCommandsTotal_, outgoingCommandsTotal_);

        incomingCommandsTotal_.clear();

        //send..
        if (outgoingCommandsMutex.try_lock())
        {
            udpTransmitter->sendMessages(outgoingCommands);
            clearOutgoingCommands();
            outgoingCommandsMutex.unlock();
        }
        outgoingCommandsTotal_.clear();
        if (connectionType == ConnectionType::CONNECTION_TCP || connectionType == ConnectionType::CONNECTION_TCP_DISABLE_UDP)
        {
            runUDP = false;
        }

        profileTimer.endLoopTime();
        long totalTime = profileTimer.getTotalLoopTime();
        if(totalTime < maxDuration)
        {
            int delay = maxDuration - totalTime;
            if(delay > 0)
            {
                std::this_thread::sleep_for(std::chrono::microseconds(delay));
            }
        }
    }
}

void AccerionSensor::runTCPCommunication()
{
    APIProfileTimer profileTimer("AccerionSensor TCP thread", true);
    float maxDuration = 1e6/NetworkConstants::APIThroughput;

    std::vector<Command> incomingCommandsTotal_;
    std::vector<Command> outgoingCommandsTotal_;
    std::vector<uint8_t> receivedMSG_;
    EnabledMessageType tcpMsgType = EnabledMessageType::INACTIVE;
    switch (connectionType)
    {
    case ConnectionType::CONNECTION_TCP:
        tcpMsgType = EnabledMessageType::BOTH;
        break;
    case ConnectionType::CONNECTION_UDP_BROADCAST:
        tcpMsgType = EnabledMessageType::INACTIVE;
        break;
    case ConnectionType::CONNECTION_UDP_UNICAST:
        tcpMsgType = EnabledMessageType::INACTIVE;
        break;
    case ConnectionType ::CONNECTION_TCP_DISABLE_UDP:
        tcpMsgType = EnabledMessageType::BOTH;
        break;
    }

    if (tcpMsgType!=EnabledMessageType::INACTIVE) {
        outgoingCommandsMutex.lock();
        outgoingCommands.emplace_back(CMD_SET_TCPIP_RECEIVER,
                                      TCPIPReceiverCommand(CMD_SET_TCPIP_RECEIVER, localIP_.first, localIP_.second,
                                                           localIP_.third, localIP_.fourth, tcpMsgType).serialize());
        outgoingCommandsMutex.unlock();
    }

    tcpClient->connectToServer();
    if(!tcpClient->getConnected())
    {
        std::cout << "ERROR IN TCP CONNECTION" << std::endl;
    }
    while (runTCP)
    {
        profileTimer.startLoopTime();
        //receive
        while (tcpClient->receiveMessage())
        {
            if (!lastMessageWasBroken_)
            {
                receivedMSG_.clear();
            }
            receivedMSG_.insert(receivedMSG_.end(), tcpClient->getReceivedMessage(), tcpClient->getReceivedMessage() + tcpClient->getReceivedNumOfBytes());
            receivedCommand_.clear();
            parseMessage(incomingCommandsTotal_, receivedMSG_);
        }

        readMessages(incomingCommandsTotal_, outgoingCommands);

        incomingCommandsTotal_.clear();

        //send..
        if (outgoingCommandsMutex.try_lock())
        {
            tcpClient->sendMessages(outgoingCommands);
            clearOutgoingCommands();
            outgoingCommandsMutex.unlock();
        }
        outgoingCommandsTotal_.clear();
        if (tcpMsgType == EnabledMessageType::INACTIVE) // TCP OR FORCE TCP
        {
            runTCP = false;
        }

        profileTimer.endLoopTime();
        long totalTime = profileTimer.getTotalLoopTime();
        if(totalTime < maxDuration)
        {
            int delay = maxDuration - totalTime;
            if(delay > 0)
            {
                std::this_thread::sleep_for(std::chrono::microseconds(delay));
            }
        }
    }
    delete tcpClient;
}

void AccerionSensor::readMessages(std::vector<Command> &incomingCommands, std::vector<Command> &outgoingCommands)
{
    auto it = incomingCommands.begin();
    while (it != incomingCommands.end())
    {
        receivedCommandID_ = (*it).commandID_;
        receivedCommand_ = (*it).command_;
        
        // if (debugMode_)
        // {
        //     std::cout << "[TCPLogUpdateManager] readMessages, Received command number is := " << std::hex << +receivedCommandID_ << std::dec << std::endl;
        //     std::cout << "[TCPLogUpdateManager] readMessages, Received command data is := ";
        //     for (unsigned int i = 0; i < receivedCommand_.size(); ++i)
        //         std::cout << receivedCommand_[i];
        //     std::cout << std::endl;
        // }

        /*Check command, set modes according to command*/
        switch (receivedCommandID_)
        {
        case CommandIDs::PRD_HEARTBEAT_INFO:
            outputHeartBeat(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_FILE:
            if(*((uint8_t*)&receivedCommand_[4]) == TransferFileType::G2O_FILE)
            {
                outgoingCommandsMutex.lock();
                g2oExportTransferHelper_.incomingFileAck(receivedCommand_, outgoingCommands);
                outgoingCommandsMutex.unlock();
            }
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_PROGRESS:
            acknowledgeGenericProgress(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_UPLOAD_FILE:
            std::cout << "ACK RECEIVED.. " << std::endl;
            if(*((uint8_t*)&receivedCommand_[0]) == TransferFileType::G2O_FILE)
            {
                outgoingCommandsMutex.lock();
                g2oUploadTransferHelper_.outgoingFileAck(receivedCommand_, outgoingCommands);
                outgoingCommandsMutex.unlock();
            }
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SENSOR_MOUNT_POSE:
            acknowledgeMountPose(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_NEW_POSITION_IS_SET:
            acknowledgeSensorPose(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_CORRECTED_POSE_DATA_LIGHT:
            outputCorrectedPose(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_UNCORRECTED_POSE_DATA:
            outputUncorrectedPose(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_DIAGNOSTICS:
            outputDiagnostics(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::INT_DRIFT_CORRECTION_DONE:
            outputDriftCorrection(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_QUALITY_ESTIMATE:
            outputQualityEstimate(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_LINE_FOLLOWER:
            outputLineFollowerData(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_SIGNATURE_MARKER:
            outputMarkerPosPacket(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_LEARNING_MODE:
            acknowledgeMappingToggle(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_DRIFT_CORRECTION_MODE:
            acknowledgeAbsoluteMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_RECORDING_MODE:
            acknowledgeRecordingMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_IDLE_MODE:
            acknowledgeIdleMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_REBOOT_MODE:
            acknowledgeRebootMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_CALIBRATION_MODE:
            acknowledgeCalibrationMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_RECORDINGS:
            acknowledgeRecordingMsg(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_IP_ADDRESS:
            acknowledgeIPAddress(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SAMPLE_RATE:
            acknowledgeSampleRate(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SERIAL_NUMBER:
            acknowledgeSerialNumber(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED:
            acknowledgeClearClusterLibrary(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SOFTWARE_VERSION:
            acknowledgeSoftwareVersion(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_TCPIP_INFO:
            acknowledgeTCPIPInformation(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_EXPERTMODE:
            acknowledgeExpertMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_RECOVERY_MODE:
            acknowledgeRecoveryMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_CLUSTER_REMOVED:
            acknowledgeRemoveCluster(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SECOND_LINE:
            acknowledgeSecondaryLineFollowerOutput(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_TIME_AND_DATE:
            acknowledgeDateTime(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_LINE_FOLLOWER_MODE:
            acknowledgeLineFollowingToggle(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_UDP_INFO:
            acknowledgeUDPSettings(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_FIND_CLOOPCLOSURES_G2O:
            acknowledgeToggleLoopClosureMsg(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_REPLACE_CLUSTER_G2O:
            acknowledgeImportG2ODone(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_CONSOLE_OUTPUT_INFO:
            outputConsoleOutputInfo(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_CLUSTER_MAP:
            retrievedMapPiece(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_PLACE_MAP:
            retrievedMapAck(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SOFTWAREHASH:
            acknowledgeSoftwareDetails(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SIGNATURE_MARKER_MAP_START_STOP:
            acknowledgeMarkerPosPacketStartStop(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_DRIFT_CORRECTION_MISSED:
            outputDriftCorrectionsMissed(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_ARUCO_MARKER_DETECTION_MODE:
            acknowledgeToggleArucoMarkerMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::INT_ARUCO_MARKER:
            outputArucoMarker(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_MAP_LOADED:
            outputMapLoaded(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_FRAME:
            acknowledgeFrameCaptureMsg(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_INTERNAL_TRACKING_MODE:
            acknowledgeInternalTrackingToggle(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
	    case CommandIDs::ACK_SET_BUFFER_LENGTH:
            acknowledgeBufferLength(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_BUFFER_PROGRESS_INDICATOR:
            acknowledgeBufferProgress(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::INT_CLUSTER_IDS:
            outputClusterIds(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        default:
            it = incomingCommands.erase(it);
            break;
        }
    }
}

void AccerionSensor::parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_)
{
    uint8_t receivedSerialNumberData[4];

    //uint8_t receivedTotalBytesInMessageData[4];
    uint32_t receivedTotalBytesInMessage;
    uint32_t numberOfBytesLeft = receivedMessage_.size();
    uint32_t numberOfReadBytes = 0;

    if (debugMode_)
    {
        std::cout << "[AccerionSensor::parseMessages] numberOfBytesLeft is " << numberOfBytesLeft << std::endl;
        std::cout << "[AccerionSensor::parseMessages] receivedMessage_. Length is " << receivedMessage_.size() << std::endl;
    }

    while (numberOfBytesLeft > 0)
    {
        receivedCommand_.clear();

        if (numberOfBytesLeft >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
        {
            for (unsigned int i = 0; i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i++)
            {
                /*Get Serial Number*/
                if (i >= CommandFields::SERIAL_NUMBER_BEGIN_BYTE && i < CommandFields::SERIAL_NUMBER_BEGIN_BYTE + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH)
                {
                    receivedSerialNumberData[i] = receivedMessage_[i + numberOfReadBytes];
                }
                /*Get Command Number*/
                else if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE && i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
                {
                    receivedCommandID_ = receivedMessage_[i + numberOfReadBytes];
                    if (debugMode_)
                    {
                        std::cout << "[AccerionSensor::parseMessages] Received command: " << std::hex << +receivedCommandID_ << std::dec << std::endl;
                    }
                }
            }
            // Extract number of bytes in message
            if (receivedCommandID_ != CMD_PLACE_MAP && receivedCommandID_ != ACK_CONSOLE_OUTPUT_INFO && receivedCommandID_ != ACK_CLUSTER_MAP && receivedCommandID_ != ACK_RECORDINGS && receivedCommandID_ != ACK_FRAME && receivedCommandID_ != ACK_FILE && receivedCommandID_ != INT_CLUSTER_IDS)
            {
                if(receivedCommandID_ == STR_SIGNATURE_MARKER)
                {
                    uint8_t numberOfMarkers[2];
                    numberOfMarkers[0] = receivedMessage_[COMMAND_ID_BEGIN_BYTE + COMMAND_ID_LENGTH + numberOfReadBytes];
                    numberOfMarkers[1] = receivedMessage_[COMMAND_ID_BEGIN_BYTE + COMMAND_ID_LENGTH + numberOfReadBytes + 1];
                    uint16_t markers = ((uint16_t)numberOfMarkers[0] << 8) | numberOfMarkers[1];
                    receivedTotalBytesInMessage = (23 * markers) + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + 2;
                    if(receivedTotalBytesInMessage == 0)
                    {
                        return;
                    }

                }
                else
                {
                    auto iter = commandValues.find(receivedCommandID_);
                    if (iter != commandValues.end())
                    {
                        receivedTotalBytesInMessage = std::get<1>(iter->second); //std::get<1>(commandValues.at(receivedCommandID_));
                    }
                    else
                    {
                        if (debugMode_)
                        {
                            std::cout << "[AccerionSensor] Unknown Command received: " << std::hex << +receivedCommandID_ << std::dec << std::endl;
                        }
                        receivedTotalBytesInMessage = 0;
                    }
                }
            }
            else // variable length messages
            {
                uint8_t receivedNumOfBytesData[4];
                receivedNumOfBytesData[0] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes];
                receivedNumOfBytesData[1] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 1];
                receivedNumOfBytesData[2] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 2];
                receivedNumOfBytesData[3] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 3];

                receivedTotalBytesInMessage = ntohl(*((uint32_t *)&receivedNumOfBytesData));
                if (debugMode_)
                {
                    std::cout << "[AccerionSensor::parseMessages] Received length: " << receivedTotalBytesInMessage << std::endl;
                }
            }
        }
        else
        {
            if (debugMode_)
            {
                std::cout << "[AccerionSensor::parseMessages] Received message was broken 1" << std::endl;
            }

            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }

        if (receivedTotalBytesInMessage <= numberOfBytesLeft)
        {
            for (unsigned int i = CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i < receivedTotalBytesInMessage; i++)
            {
                /*Get Command Data*/
                if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH && i < receivedTotalBytesInMessage - 1)
                {
                    receivedCommand_.push_back(receivedMessage_[i + numberOfReadBytes]);
                }
                /*Get CRC8 Data*/
                else
                {
                    receivedCRC8_ = receivedMessage_[i + numberOfReadBytes];
                }
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());

            if (debugMode_)
            {
                std::cout << "[AccerionSensor::parseMessages] Received message " << std::hex << +receivedCommandID_ << std::dec << "was broken 2 with no of bytesleft: " << numberOfBytesLeft << std::endl;
                std::cout << "[AccerionSensor::parseMessages]receivedMessage_. Length after broken 2 is " << receivedMessage_.size() << std::endl;
            }
            
            return;//break;
        }

        lastMessageWasBroken_ = false;

        /*Check serial number*/
        receivedSerialNumber_ = ntohl(*((uint32_t *)&receivedSerialNumberData));

        uint8_t calculatedCRC8 = crc8_.crcFast((uint8_t *)receivedMessage_.data() + numberOfReadBytes, receivedTotalBytesInMessage - 1);
        if (receivedSerialNumber_ != sensorSerialNumber_ && receivedSerialNumber_ != DEFAULT_SERIAL_NUMBER && sensorSerialNumber_ != DEFAULT_SERIAL_NUMBER)
        {
            if (debugMode_)
            {
                std::cout << "Received Message is not for this Jupiter, which has serial number := " << std::hex << sensorSerialNumber_ << " it is intended for serial number := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            }
            messageReady_ = false;
            return;
        }
        if (receivedCRC8_ != calculatedCRC8)
        {
            if (debugMode_)
            {
                std::cout << "Received Message does not have the correct crc8 code, it has the wrong code := " << std::hex << +receivedCRC8_ << std::dec << std::endl;
            }
            messageReady_ = false;
            return;
        }

        if (debugMode_)
        {
            std::cout << "Command length of the to be inserted command: " << receivedCommand_.size() << std::endl;
        }

        commands.emplace_back(receivedCommandID_, receivedCommand_);

        if (debugMode_)
        {
            std::cout << "Received serialNumber is := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            std::cout << "Received command number is := " << std::hex << +receivedCommandID_ << std::dec << std::endl;
        }

        numberOfReadBytes += receivedTotalBytesInMessage;
        numberOfBytesLeft -= receivedTotalBytesInMessage;
        receivedCommand_.clear();
    }
}

void AccerionSensor::setAbsoluteModeCallback(_acknowledgementCallBack amCallback)
{
    absoluteModeCallBack = amCallback;
}

void AccerionSensor::toggleAbsoluteMode(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_DRIFT_CORRECTION_MODE, BooleanCommand(CMD_SET_DRIFT_CORRECTION_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleAbsoluteMode(bool on, _acknowledgementCallBack amCallback)
{
    setAbsoluteModeCallback(amCallback);
    toggleAbsoluteMode(on);
}

int AccerionSensor::toggleAbsoluteModeBlocking(bool on)
{
    toggleAbsoluteMode(on);

    std::unique_lock<std::mutex> lck(absoluteModeAckMutex);
    if (absoluteModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedAbsoluteModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeAbsoluteMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (absoluteModeCallBack)
    {
        absoluteModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(absoluteModeAckMutex);
    receivedAbsoluteModeAck = ack;
    absoluteModeAckCV.notify_all();
}

void AccerionSensor::setRecordingModeCallback(_acknowledgementCallBack amCallback)
{
    recordingModeCallBack = amCallback;
}

void AccerionSensor::toggleRecordingMode(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_RECORDING_MODE, BooleanCommand(CMD_SET_RECORDING_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleRecordingMode(bool on, _acknowledgementCallBack amCallback)
{
    setRecordingModeCallback(amCallback);
    toggleRecordingMode(on);
}

int AccerionSensor::toggleRecordingModeBlocking(bool on)
{
    toggleRecordingMode(on);

    std::unique_lock<std::mutex> lck(recordingModeAckMutex);
    if (recordingModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedRecordingModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeRecordingMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (recordingModeCallBack)
    {
        recordingModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(recordingModeAckMutex);
    receivedRecordingModeAck = ack;
    recordingModeAckCV.notify_all();
}

void AccerionSensor::setIdleModeCallback(_acknowledgementCallBack amCallback)
{
    idleModeCallBack = amCallback;
}

void AccerionSensor::toggleIdleMode(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_IDLE_MODE, BooleanCommand(CMD_SET_IDLE_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleIdleMode(bool on, _acknowledgementCallBack amCallback)
{
    setIdleModeCallback(amCallback);
    toggleIdleMode(on);
}

int AccerionSensor::toggleIdleModeBlocking(bool on)
{
    toggleIdleMode(on);

    std::unique_lock<std::mutex> lck(idleModeAckMutex);
    if (idleModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedIdleModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeIdleMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (idleModeCallBack)
    {
        idleModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(idleModeAckMutex);
    receivedIdleModeAck = ack;
    idleModeAckCV.notify_all();
}

void AccerionSensor::setRebootModeCallback(_acknowledgementCallBack amCallback)
{
    rebootModeCallBack = amCallback;
}

void AccerionSensor::toggleRebootMode(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_REBOOT_MODE, BooleanCommand(CMD_SET_REBOOT_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleRebootMode(bool on, _acknowledgementCallBack amCallback)
{
    setRebootModeCallback(amCallback);
    toggleRebootMode(on);
}

int AccerionSensor::toggleRebootModeBlocking(bool on)
{
    toggleRebootMode(on);

    std::unique_lock<std::mutex> lck(rebootModeAckMutex);
    if (rebootModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedRebootModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeRebootMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (rebootModeCallBack)
    {
        rebootModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(rebootModeAckMutex);
    receivedRebootModeAck = ack;
    rebootModeAckCV.notify_all();
}

void AccerionSensor::toggleCalibrationMode(bool on, _acknowledgementCallBack amCallback)
{
    calibrationModeCallBack = amCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_CALIBRATION_MODE, BooleanCommand(CMD_SET_CALIBRATION_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleCalibrationModeBlocking(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_CALIBRATION_MODE, BooleanCommand(CMD_SET_CALIBRATION_MODE, on).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(calibrationModeAckMutex);
    if (calibrationModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedCalibrationModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeCalibrationMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (calibrationModeCallBack)
    {
        calibrationModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(calibrationModeAckMutex);
    receivedCalibrationModeAck = ack;
    calibrationModeAckCV.notify_all();
}

void AccerionSensor::setGetIPAddressCallback(_ipAddressCallBack ipCallback)
{
    ipAddressCallBack = ipCallback;
}

void AccerionSensor::getIPAddress()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_IP_ADDRESS, EmptyCommand(CMD_GET_IP_ADDRESS).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::getIPAddress(_ipAddressCallBack ipCallback)
{
    setGetIPAddressCallback(ipCallback);
    getIPAddress();
}

IPAddressExtended AccerionSensor::getIPAddressBlocking()
{
    getIPAddress();

    std::unique_lock<std::mutex> lck(ipAddressAckMutex);
    if (ipAddressAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        IPAddressExtended empty;
        empty.staticIPAddress.first = 0;
        empty.staticIPAddress.second = 0;
        empty.staticIPAddress.third = 0;
        empty.staticIPAddress.fourth = 0;
        empty.staticNetmask.first = 0;
        empty.staticNetmask.second = 0;
        empty.staticNetmask.third = 0;
        empty.staticNetmask.fourth = 0;
        empty.dynamicIPAddress.first = 0;
        empty.dynamicIPAddress.second = 0;
        empty.dynamicIPAddress.third = 0;
        empty.dynamicIPAddress.fourth = 0;
        empty.dynamicNetmask.first = 0;
        empty.dynamicNetmask.second = 0;
        empty.dynamicNetmask.third = 0;
        empty.dynamicNetmask.fourth = 0;
        empty.defaultGateway.first = 0;
        empty.defaultGateway.second = 0;
        empty.defaultGateway.third = 0;
        empty.defaultGateway.fourth = 0;
        return empty;
    }

    return receivedIPAddress;
}

void AccerionSensor::acknowledgeIPAddress(std::vector<uint8_t> data)
{
    IPAddressExtended ip;
    ip.staticIPAddress.first = data[0];
    ip.staticIPAddress.second = data[1];
    ip.staticIPAddress.third = data[2];
    ip.staticIPAddress.fourth = data[3];
    ip.staticNetmask.first = data[4];
    ip.staticNetmask.second = data[5];
    ip.staticNetmask.third = data[6];
    ip.staticNetmask.fourth = data[7];
    ip.dynamicIPAddress.first = data[8];
    ip.dynamicIPAddress.second = data[9];
    ip.dynamicIPAddress.third = data[10];
    ip.dynamicIPAddress.fourth = data[11];
    ip.dynamicNetmask.first = data[12];
    ip.dynamicNetmask.second = data[13];
    ip.dynamicNetmask.third = data[14];
    ip.dynamicNetmask.fourth = data[15];
    ip.defaultGateway.first = data[16];
    ip.defaultGateway.second = data[17];
    ip.defaultGateway.third = data[18];
    ip.defaultGateway.fourth = data[19];

    if (ipAddressCallBack)
    {
        ipAddressCallBack(ip);
    }
    std::unique_lock<std::mutex> lck(ipAddressAckMutex);
    receivedIPAddress = ip;
    ipAddressAckCV.notify_all();
}

void AccerionSensor::getSampleRate(_sampleRateCallBack srCallback)
{
    sampleRateCallBack = srCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SAMPLE_RATE, EmptyCommand(CMD_GET_SAMPLE_RATE).serialize());
    outgoingCommandsMutex.unlock();
}

SampleRate AccerionSensor::getSampleRateBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SAMPLE_RATE, EmptyCommand(CMD_GET_SAMPLE_RATE).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(sampleRateAckMutex);
    if (sampleRateAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        SampleRate empty;
        empty.sampleRateFrequency = 0;
        return empty;
    }

    return receivedSampleRate;
}

void AccerionSensor::acknowledgeSampleRate(std::vector<uint8_t> data)
{
    SampleRate sr;
    sr.sampleRateFrequency = (uint16_t)ntohs(*((uint16_t *)&data[0]));

    if (sampleRateCallBack)
    {
        sampleRateCallBack(sr);
    }
    std::unique_lock<std::mutex> lck(sampleRateAckMutex);
    receivedSampleRate = sr;
    sampleRateAckCV.notify_all();
}

void AccerionSensor::getSerialNumber(_serialNumberCallBack serialNRCallback)
{
    serialNumberCallBack = serialNRCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_ALL_SERIAL_NUMBERS, EmptyCommand(CMD_GET_ALL_SERIAL_NUMBERS).serialize());
    outgoingCommandsMutex.unlock();
}

SerialNumber AccerionSensor::getSerialNumberBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_ALL_SERIAL_NUMBERS, EmptyCommand(CMD_GET_ALL_SERIAL_NUMBERS).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(serialNumberAckMutex);
    if (serialNumberAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        SerialNumber sn;
        sn.serialNumber = 0;
        return sn;
    }

    return receivedSerialNumber;
}

void AccerionSensor::acknowledgeSerialNumber(std::vector<uint8_t> data)
{
    SerialNumber sn;
    sn.serialNumber = ((uint32_t)ntohl(*((uint32_t *)&data[0])));

    if (serialNumberCallBack)
    {
        serialNumberCallBack(sn);
    }
    std::unique_lock<std::mutex> lck(serialNumberAckMutex);
    receivedSerialNumber = sn;
    serialNumberAckCV.notify_all();
}

void AccerionSensor::setClearClusterLibraryCallback(_acknowledgementCallBack clearClusterCallback)
{
    clearClusterLibraryCallBack = clearClusterCallback;
}

void AccerionSensor::clearClusterLibrary()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY, EmptyCommand(CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::clearClusterLibrary(_acknowledgementCallBack clearClusterCallback)
{
    setClearClusterLibraryCallback(clearClusterCallback);
    clearClusterLibrary();
}

int AccerionSensor::clearClusterLibraryBlocking()
{
    clearClusterLibrary();

    std::unique_lock<std::mutex> lck(clearClusterLibraryAckMutex);
    if (clearClusterLibraryAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedClearClusterLibraryAck.value)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeClearClusterLibrary(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (clearClusterLibraryCallBack)
    {
        clearClusterLibraryCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(clearClusterLibraryAckMutex);
    receivedClearClusterLibraryAck = ack;
    clearClusterLibraryAckCV.notify_all();
}

void AccerionSensor::getAllAcknowledgements()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_ALL_ACKNOWLEDGEMENTS, EmptyCommand(CMD_GET_ALL_ACKNOWLEDGEMENTS).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setSoftwareVersionCallback(_softwareVersionCallBack svCallback)
{
    softwareVersionCallBack = svCallback;
}

void AccerionSensor::getSoftwareVersion()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SOFTWARE_VERSION, EmptyCommand(CMD_GET_SOFTWARE_VERSION).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::getSoftwareVersion(_softwareVersionCallBack svCallback)
{
    setSoftwareVersionCallback(svCallback);
    getSoftwareVersion();
}

SoftwareVersion AccerionSensor::getSoftwareVersionBlocking()
{
    getSoftwareVersion();

    std::unique_lock<std::mutex> lck(softwareVersionAckMutex);
    if (softwareVersionAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        SoftwareVersion sv;
        sv.major = 0;
        sv.minor = 0;
        sv.patch = 0;
        return sv;
    }

    return receivedSoftwareVersion;
}

void AccerionSensor::acknowledgeSoftwareVersion(std::vector<uint8_t> data)
{
    SoftwareVersion sv;
    sv.major = data[0];
    sv.minor = data[1];
    sv.patch = data[2];

    if (softwareVersionCallBack)
    {
        softwareVersionCallBack(sv);
    }
    std::unique_lock<std::mutex> lck(softwareVersionAckMutex);
    receivedSoftwareVersion = sv;
    softwareVersionAckCV.notify_all();
}

void AccerionSensor::setTCPIPInformationCallback(_tcpIPInformationCallBack ipCallback)
{
    tcpIPInformationCallBack = ipCallback;
}

void AccerionSensor::getTCPIPInformation()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_TCPIP_RECEIVER, EmptyCommand(CMD_GET_TCPIP_RECEIVER).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::getTCPIPInformation(_tcpIPInformationCallBack ipCallback)
{
    setTCPIPInformationCallback(ipCallback);
    getTCPIPInformation();
}

TCPIPInformation AccerionSensor::getTCPIPInformationBlocking()
{
    getTCPIPInformation();

    std::unique_lock<std::mutex> lck(tcpIPInformationAckMutex);
    if (tcpIPInformationAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        TCPIPInformation ti;
        ti.ipAddress.first = 0;
        ti.ipAddress.second = 0;
        ti.ipAddress.third = 0;
        ti.ipAddress.fourth = 0;
        ti.hostIPAddress.first = 0;
        ti.hostIPAddress.second = 0;
        ti.hostIPAddress.third = 0;
        ti.hostIPAddress.fourth = 0;
        ti.messageType = EnabledMessageType::INACTIVE;
        return ti;
    }

    return receivedTCPIPInformation;
}

void AccerionSensor::acknowledgeTCPIPInformation(std::vector<uint8_t> data)
{
    TCPIPInformation ti;
    ti.ipAddress.first = data[0];
    ti.ipAddress.second = data[1];
    ti.ipAddress.third = data[2];
    ti.ipAddress.fourth = data[3];
    ti.hostIPAddress.first = data[4];
    ti.hostIPAddress.second = data[5];
    ti.hostIPAddress.third = data[6];
    ti.hostIPAddress.fourth = data[7];
    ti.messageType = static_cast<EnabledMessageType>(data[8]);

    if (tcpIPInformationCallBack)
    {
        tcpIPInformationCallBack(ti);
    }
    std::unique_lock<std::mutex> lck(tcpIPInformationAckMutex);
    receivedTCPIPInformation = ti;
    tcpIPInformationAckCV.notify_all();
}

void AccerionSensor::getSensorMountPose()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SENSOR_MOUNT_POSE, EmptyCommand(CMD_GET_SENSOR_MOUNT_POSE).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::getSensorMountPose(_poseCallBack mpCallback)
{
    sensorMountPoseCallBack = mpCallback;
    getSensorMountPose();
}

Pose AccerionSensor::getSensorMountPoseBlocking()
{
    getSensorMountPose();

    std::unique_lock<std::mutex> lck(mountPoseMutex);
    if (mountPoseCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        Pose ap;
        ap.heading = 0;
        ap.x = 0;
        ap.y = 0;
        return ap;
    }

    return receivedMountPose;
}

void AccerionSensor::setExpertModeCallback(_acknowledgementCallBack emCallback)
{
    expertModeCallBack = emCallback;
}

void AccerionSensor::toggleExpertMode(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_EXPERTMODE, BooleanCommand(CMD_SET_EXPERTMODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleExpertMode(bool on, _acknowledgementCallBack emCallback)
{
    setExpertModeCallback(emCallback);
    toggleExpertMode(on);
}

int AccerionSensor::toggleExpertModeBlocking(bool on)
{
    toggleExpertMode(on);

    std::unique_lock<std::mutex> lck(expertModeAckMutex);
    if (expertModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedExpertModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeExpertMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (expertModeCallBack)
    {
        expertModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(expertModeAckMutex);
    receivedExpertModeAck = ack;
    expertModeAckCV.notify_all();
}

void AccerionSensor::setSampleRateCallback(_sampleRateCallBack srCallback)
{
    sampleRateCallBack = srCallback;
}

void AccerionSensor::setSampleRate(SampleRate rate)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_SAMPLE_RATE, UINT16Command(CMD_SET_NEW_POSE, rate.sampleRateFrequency).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setSampleRate(SampleRate rate, _sampleRateCallBack srCallback)
{
    setSampleRateCallback(srCallback);
    setSampleRate(rate);
}

int AccerionSensor::setSampleRateBlocking(SampleRate rate)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_NEW_POSE, UINT16Command(CMD_SET_NEW_POSE, rate.sampleRateFrequency).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(sampleRateAckMutex);
    if (sampleRateAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedSampleRate.sampleRateFrequency == rate.sampleRateFrequency)
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::setRecoveryModeCallback(_acknowledgementCallBack emCallback)
{
    recoveryModeCallBack = emCallback;
}

void AccerionSensor::toggleRecoveryMode(bool on, uint8_t radius)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_RECOVERY_MODE, RecoveryCommand(CMD_SET_RECOVERY_MODE, on, radius).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleRecoveryMode(bool on, uint8_t radius, _acknowledgementCallBack emCallback)
{
    setRecoveryModeCallback(emCallback);
    toggleRecoveryMode(on, radius);
}

int AccerionSensor::toggleRecoveryModeBlocking(bool on, uint8_t radius)
{
    toggleRecoveryMode(on, radius);

    std::unique_lock<std::mutex> lck(recoveryModeAckMutex);
    if (recoveryModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedRecoveryModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeRecoveryMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (recoveryModeCallBack)
    {
        recoveryModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(recoveryModeAckMutex);
    receivedRecoveryModeAck = ack;
    recoveryModeAckCV.notify_all();
}

void AccerionSensor::setRemoveClusterFromLibraryCallback(_removeClusterCallBack rclusterCallback)
{
    removeClusterCallBack = rclusterCallback;
}

void AccerionSensor::removeClusterFromLibrary(uint16_t clusterID)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_DELETE_CLUSTER, UINT16Command(CMD_DELETE_CLUSTER, clusterID).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::removeClusterFromLibrary(uint16_t clusterID, _removeClusterCallBack rclusterCallback)
{
    setRemoveClusterFromLibraryCallback(rclusterCallback);
    removeClusterFromLibrary(clusterID);
}

int AccerionSensor::removeClusterFromLibraryBlocking(uint16_t clusterID)
{
    removeClusterFromLibrary(clusterID);

    std::unique_lock<std::mutex> lck(removeClusterAckMutex);
    if (removeClusterAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedRemoveClusterAck == clusterID)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeRemoveCluster(std::vector<uint8_t> data)
{
    auto clusterID = (uint16_t)ntohs(*((uint16_t *)&data[0]));

    if (removeClusterCallBack)
    {
        removeClusterCallBack(clusterID);
    }
    std::unique_lock<std::mutex> lck(removeClusterAckMutex);
    receivedRemoveClusterAck = clusterID;
    removeClusterAckCV.notify_all();
}

void AccerionSensor::setSecondaryLineFollowerOutputCallback(_secondaryLineFollowerCallBack slfCallback)
{
    secondaryLineFollowerCallBack = slfCallback;
}

void AccerionSensor::getSecondaryLineFollowerOutput(uint16_t clusterID)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SECOND_LINE, UINT16Command(CMD_SECOND_LINE, clusterID).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::getSecondaryLineFollowerOutput(uint16_t clusterID, _secondaryLineFollowerCallBack slfCallback)
{
    setSecondaryLineFollowerOutputCallback(slfCallback);
    getSecondaryLineFollowerOutput(clusterID);
}

LineFollowerData AccerionSensor::getSecondaryLineFollowerOutputBlocking(uint16_t clusterID)
{
    getSecondaryLineFollowerOutput(clusterID);

    std::unique_lock<std::mutex> lck(secondaryLineFollowerOutputMutex);
    if (secondaryLineFollowerOutputCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        LineFollowerData lfd;
        lfd.timeStamp = 0;
        lfd.pose.x = 0;
        lfd.pose.y = 0;
        lfd.pose.heading = 0;
        lfd.closestPointX = 0;
        lfd.closestPointY = 0;
        lfd.reserved = 0;
        lfd.clusterID = 0;
        return lfd;
    }

    return receivedSecondaryLineFollowerOutput;
}

void AccerionSensor::acknowledgeSecondaryLineFollowerOutput(std::vector<uint8_t> data)
{
    LineFollowerData lfd;
    lfd.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    lfd.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    lfd.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    lfd.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    lfd.closestPointX = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    lfd.closestPointY = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    lfd.reserved = (int32_t)ntohl(*((int32_t *)&data[28]));
    lfd.clusterID = (uint16_t)ntohs(*((uint16_t *)&data[32]));
    if (secondaryLineFollowerCallBack)
    {
        secondaryLineFollowerCallBack(lfd);
    }
    std::unique_lock<std::mutex> lck(secondaryLineFollowerOutputMutex);
    receivedSecondaryLineFollowerOutput = lfd;
    secondaryLineFollowerOutputCV.notify_all();
}

void AccerionSensor::setIPAddressCallback(_ipAddressCallBack ipCallback)
{
    ipAddressCallBack = ipCallback;
}

void AccerionSensor::setIPAddress(IPAddress ip)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_IP_ADDRESS, SetIPCommand(CMD_SET_IP_ADDRESS, ip.ipAddress.first, ip.ipAddress.second, ip.ipAddress.third, ip.ipAddress.fourth, ip.netmask.first, ip.netmask.second, ip.netmask.third, ip.netmask.fourth, ip.gateway.first, ip.gateway.second, ip.gateway.third, ip.gateway.fourth).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setIPAddress(IPAddress ip, _ipAddressCallBack ipCallback)
{
    setIPAddressCallback(ipCallback);
    setIPAddress(ip);
}

IPAddressExtended AccerionSensor::setIPAddressBlocking(IPAddress ip)
{
    setIPAddress(ip);

    std::unique_lock<std::mutex> lck(ipAddressAckMutex);
    if (ipAddressAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        IPAddressExtended empty;
        empty.staticIPAddress.first = 0;
        empty.staticIPAddress.second = 0;
        empty.staticIPAddress.third = 0;
        empty.staticIPAddress.fourth = 0;
        empty.staticNetmask.first = 0;
        empty.staticNetmask.second = 0;
        empty.staticNetmask.third = 0;
        empty.staticNetmask.fourth = 0;
        empty.dynamicIPAddress.first = 0;
        empty.dynamicIPAddress.second = 0;
        empty.dynamicIPAddress.third = 0;
        empty.dynamicIPAddress.fourth = 0;
        empty.dynamicNetmask.first = 0;
        empty.dynamicNetmask.second = 0;
        empty.dynamicNetmask.third = 0;
        empty.dynamicNetmask.fourth = 0;
        empty.defaultGateway.first = 0;
        empty.defaultGateway.second = 0;
        empty.defaultGateway.third = 0;
        empty.defaultGateway.fourth = 0;
        return empty;
    }

    return receivedIPAddress;
}

void AccerionSensor::setDateTimeCallback(_dateTimeCallBack dtCallback)
{
    dateTimeCallBack = dtCallback;
}

void AccerionSensor::setDateTime(DateTime dt)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_TIME_DATE, DateTimeCommand(CMD_SET_TIME_DATE, dt.day, dt.month, dt.year, dt.hours, dt.minutes, dt.seconds).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setDateTime(DateTime dt, _dateTimeCallBack dtCallback)
{
    setDateTimeCallback(dtCallback);
    setDateTime(dt);
}

int AccerionSensor::setDateTimeBlocking(DateTime dt)
{
    setDateTime(dt);

    std::unique_lock<std::mutex> lck(dateTimeMutex);
    if (dateTimeCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedDateTimeAck.day == dt.day && receivedDateTimeAck.month == dt.month && receivedDateTimeAck.year == dt.year && receivedDateTimeAck.hours == dt.hours && receivedDateTimeAck.minutes == dt.minutes && receivedDateTimeAck.seconds == dt.seconds)
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::acknowledgeDateTime(std::vector<uint8_t> data)
{
    DateTime dt;
    dt.year = (uint16_t)ntohs(*((uint16_t *)&data[0]));
    dt.month = data[2];
    dt.day = data[3];
    dt.hours = data[4];
    dt.minutes = data[5];
    dt.seconds = data[6];

    if (dateTimeCallBack)
    {
        dateTimeCallBack(dt);
    }
    std::unique_lock<std::mutex> lck(dateTimeMutex);
    receivedDateTimeAck = dt;
    dateTimeCV.notify_all();
}

void AccerionSensor::setSensorPoseCallback(_poseCallBack mpCallback)
{
    sensorPoseCallBack = mpCallback;
}

void AccerionSensor::setSensorPose(Pose mountPoseStruct)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_NEW_POSE, PoseCommand(CMD_SET_NEW_POSE, mountPoseStruct.x, mountPoseStruct.y, mountPoseStruct.heading).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setSensorPose(Pose poseStruct, _poseCallBack mpCallback)
{
    setSensorPoseCallback(mpCallback);
    setSensorPose(poseStruct);
}

int AccerionSensor::setSensorPoseBlocking(Pose poseStruct)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_NEW_POSE, PoseCommand(CMD_SET_NEW_POSE, poseStruct.x, poseStruct.y, poseStruct.heading).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(sensorPoseMutex);
    if (sensorPoseCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (round(receivedSensorPose.x / 1000000.0) == round(poseStruct.x / 1000000.0) && round(receivedSensorPose.y / 1000000.0) == round(poseStruct.y / 1000000.0) && round(receivedSensorPose.heading / 100.0) == round(poseStruct.heading / 100.0))
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::acknowledgeSensorPose(std::vector<uint8_t> data)
{
    Pose sensorPoseStruct;
    sensorPoseStruct.x = ((int32_t)ntohl(*((int32_t *)&data[0]))) / 1000000.0;
    sensorPoseStruct.y = ((int32_t)ntohl(*((int32_t *)&data[4]))) / 1000000.0;
    sensorPoseStruct.heading = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 100.0;
    if (sensorPoseCallBack)
    {
        sensorPoseCallBack(sensorPoseStruct);
    }
    std::unique_lock<std::mutex> lck(sensorPoseMutex);
    receivedSensorPose = sensorPoseStruct;
    sensorPoseCV.notify_all();
}

void AccerionSensor::setSensorMountPoseCallback(_poseCallBack mpCallback)
{
    sensorMountPoseCallBack = mpCallback;
}

void AccerionSensor::setSensorMountPose(Pose mountPoseStruct)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_SENSOR_MOUNT_POSE, PoseCommand(CMD_SET_SENSOR_MOUNT_POSE, mountPoseStruct.x, mountPoseStruct.y, mountPoseStruct.heading).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setSensorMountPose(Pose mountPoseStruct, _poseCallBack mpCallback)
{
    setSensorMountPoseCallback(mpCallback);
    setSensorMountPose(mountPoseStruct);
}

int AccerionSensor::setSensorMountPoseBlocking(Pose mountPoseStruct)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_SENSOR_MOUNT_POSE, PoseCommand(CMD_SET_SENSOR_MOUNT_POSE, mountPoseStruct.x, mountPoseStruct.y, mountPoseStruct.heading).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(mountPoseMutex);
    if (mountPoseCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (round(receivedMountPose.x / 1000000.0) == round(mountPoseStruct.x / 1000000.0) && round(receivedMountPose.y / 1000000.0) == round(mountPoseStruct.y / 1000000.0) && round(receivedMountPose.heading / 100.0) == round(mountPoseStruct.heading / 100.0))
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::acknowledgeMountPose(std::vector<uint8_t> data)
{
    Pose mountPoseStruct;
    mountPoseStruct.x = ((int32_t)ntohl(*((int32_t *)&data[0]))) / 1000000.0;
    mountPoseStruct.y = ((int32_t)ntohl(*((int32_t *)&data[4]))) / 1000000.0;
    mountPoseStruct.heading = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 100.0;
    if (sensorMountPoseCallBack)
    {
        sensorMountPoseCallBack(mountPoseStruct);
    }
    std::unique_lock<std::mutex> lck(mountPoseMutex);
    receivedMountPose = mountPoseStruct;
    mountPoseCV.notify_all();
}

void AccerionSensor::setPoseAndCovariance(InputPose inputPose)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_POSE_AND_COVARIANCE, PoseAndCovarianceCommand(CMD_SET_POSE_AND_COVARIANCE, inputPose.timeOffsetInUsec, inputPose.pose.x, inputPose.pose.y, inputPose.pose.heading, inputPose.standardDeviation.x, inputPose.standardDeviation.y, inputPose.standardDeviation.theta).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setTCPIPReceiverCallback(_tcpIPInformationCallBack tcpIPCallback)
{
    tcpIPInformationCallBack = tcpIPCallback;
}

void AccerionSensor::setTCPIPReceiver(Address ipAddr, EnabledMessageType messageType)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_TCPIP_RECEIVER, TCPIPReceiverCommand(CMD_SET_TCPIP_RECEIVER, ipAddr.first, ipAddr.second, ipAddr.third, ipAddr.fourth, messageType).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setTCPIPReceiver(Address ipAddr, EnabledMessageType messageType, _tcpIPInformationCallBack tcpIPCallback)
{
    setTCPIPReceiverCallback(tcpIPCallback);
    setTCPIPReceiver(ipAddr, messageType);
}

int AccerionSensor::setTCPIPReceiverBlocking(Address ipAddr, EnabledMessageType messageType) /** < Returns -1 on error, 0 if it is off, 1 if it is on */
{
    setTCPIPReceiver(ipAddr, messageType);

    std::unique_lock<std::mutex> lck(tcpIPInformationAckMutex);
    if (tcpIPInformationAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedTCPIPInformation.hostIPAddress.first == ipAddr.first && receivedTCPIPInformation.hostIPAddress.second == ipAddr.second && receivedTCPIPInformation.hostIPAddress.third == ipAddr.third && receivedTCPIPInformation.hostIPAddress.fourth == ipAddr.fourth && receivedTCPIPInformation.messageType == messageType)
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::setMappingCallback(_acknowledgementCallBack mappingCallback)
{
    toggleMappingCallBack = mappingCallback;
}

void AccerionSensor::toggleMapping(bool on, uint16_t clusterID)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_START_MARKERLESS_LEARNING, ToggleMappingCommand(CMD_START_MARKERLESS_LEARNING, on, clusterID).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleMapping(bool on, uint16_t clusterID, _acknowledgementCallBack mappingCallback)
{
    setMappingCallback(mappingCallback);
    toggleMapping(on, clusterID);
}

int AccerionSensor::toggleMappingBlocking(bool on, uint16_t clusterID)
{
    toggleMapping(on, clusterID);

    std::unique_lock<std::mutex> lck(mappingAckMutex);
    if (mappingAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedMappingAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeMappingToggle(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (toggleMappingCallBack)
    {
        toggleMappingCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(mappingAckMutex);
    receivedMappingAck = ack;
    mappingAckCV.notify_all();
}

void AccerionSensor::setLineFollowingCallback(_acknowledgementCallBack tlfCallback)
{
    toggleLineFollowingCallBack = tlfCallback;
}

void AccerionSensor::toggleLineFollowing(bool on, uint16_t clusterID)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_START_LINE_FOLLOWER, ToggleMappingCommand(CMD_START_LINE_FOLLOWER, on, clusterID).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleLineFollowing(bool on, uint16_t clusterID, _acknowledgementCallBack tlfCallback)
{
    setLineFollowingCallback(tlfCallback);
    toggleLineFollowing(on, clusterID);
}

int AccerionSensor::toggleLineFollowingBlocking(bool on, uint16_t clusterID)
{
    toggleLineFollowing(on, clusterID);

    std::unique_lock<std::mutex> lck(lineFollowingAckMutex);
    if (lineFollowingAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedLineFollowingAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeLineFollowingToggle(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (toggleLineFollowingCallBack)
    {
        toggleLineFollowingCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(lineFollowingAckMutex);
    receivedLineFollowingAck = ack;
    lineFollowingAckCV.notify_all();
}

void AccerionSensor::setUDPSettingsCallback(_setUDPSettingsCallBack udpCallBack)
{
    setUDPSettingsCallBack = udpCallBack;
}

void AccerionSensor::setUDPSettings(UDPInfo udpInfo)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_UDP_SETTINGS, UDPSettingsCommand(CMD_SET_UDP_SETTINGS, udpInfo.ipAddress.first, udpInfo.ipAddress.second, udpInfo.ipAddress.third, udpInfo.ipAddress.fourth, udpInfo.messageType, udpInfo.strategy).serialize());
    outgoingCommandsMutex.unlock();
    // if(!runUDP)
    // {
    //     if (outgoingCommandsMutex.try_lock())
    //     {
    //         udpTransmitter->sendMessages(outgoingCommands);
    //         clearOutgoingCommands();
    //         outgoingCommandsMutex.unlock();
    //     }
    // }
}

void AccerionSensor::setUDPSettings(UDPInfo udpInfo, _setUDPSettingsCallBack udpCallBack)
{
    setUDPSettingsCallback(udpCallBack);
    setUDPSettings(udpInfo);
}

int AccerionSensor::setUDPSettingsBlocking(UDPInfo udpInfo)
{
    setUDPSettings(udpInfo);

    std::unique_lock<std::mutex> lck(setUDPSettingsAckMutex);
    if (setUDPSettingsAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedSetUDPSettingsAck.ipAddress.first == udpInfo.ipAddress.first && receivedSetUDPSettingsAck.ipAddress.second == udpInfo.ipAddress.second && receivedSetUDPSettingsAck.ipAddress.third == udpInfo.ipAddress.third && receivedSetUDPSettingsAck.ipAddress.fourth == udpInfo.ipAddress.fourth && receivedSetUDPSettingsAck.messageType == udpInfo.messageType && receivedSetUDPSettingsAck.strategy == udpInfo.strategy)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeUDPSettings(std::vector<uint8_t> data)
{
    UDPInfo udpInfo;
    udpInfo.messageType = static_cast<EnabledMessageType>(data[0]);
    udpInfo.strategy = static_cast<UDPStrategy>(data[1]);
    udpInfo.ipAddress.first = data[2];
    udpInfo.ipAddress.second = data[3];
    udpInfo.ipAddress.third = data[4];
    udpInfo.ipAddress.fourth = data[5];

    if (setUDPSettingsCallBack)
    {
        setUDPSettingsCallBack(udpInfo);
    }
    std::unique_lock<std::mutex> lck(setUDPSettingsAckMutex);
    receivedSetUDPSettingsAck = udpInfo;
    setUDPSettingsAckCV.notify_all();
}

void AccerionSensor::subscribeToHeartBeat(_heartBeatCallBack hbCallback)
{
    heartBeatCallBack = hbCallback;
}

void AccerionSensor::outputHeartBeat(std::vector<uint8_t> data)
{
    HeartBeat hb;
    hb.ipAddrFirst = data[0];
    hb.ipAddrSecond = data[1];
    hb.ipAddrThird = data[2];
    hb.ipAddrFourth = data[3];
    hb.uniFirst = data[4];
    hb.uniSecond = data[5];
    hb.uniThird = data[6];
    hb.uniFourth = data[7];

    if (heartBeatCallBack)
    {
        heartBeatCallBack(hb);
    }
}

void AccerionSensor::subscribeToCorrectedPose(_correctedPoseCallBack cpCallback)
{
    correctedPoseCallBack = cpCallback;
}

void AccerionSensor::outputCorrectedPose(std::vector<uint8_t> data)
{
    CorrectedPose cp;

    cp.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    cp.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    cp.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    cp.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    cp.xVel = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    cp.yVel = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    cp.thVel = ((int16_t)ntohl(*((int16_t *)&data[28]))) / 100.0;

    cp.standardDeviation.x = ((uint32_t)ntohl(*((uint32_t *)&data[30]))) / 1000000.0;
    cp.standardDeviation.y = ((uint32_t)ntohl(*((uint32_t *)&data[34]))) / 1000000.0;
    cp.standardDeviation.theta = ((uint32_t)ntohl(*((uint32_t *)&data[38]))) / 100.0;

    cp.standardDeviationVelocity.x = ((uint32_t)ntohl(*((uint32_t *)&data[42]))) / 1000000.0;
    cp.standardDeviationVelocity.y = ((uint32_t)ntohl(*((uint32_t *)&data[46]))) / 1000000.0;
    cp.standardDeviationVelocity.theta = ((uint32_t)ntohl(*((uint32_t *)&data[50]))) / 100.0;

    cp.qualityEstimate = data[54];

    if (correctedPoseCallBack)
    {
        correctedPoseCallBack(cp);
    }
}

void AccerionSensor::subscribeToUncorrectedPose(_uncorrectedPoseCallBack upCallback)
{
    uncorrectedPoseCallBack = upCallback;
}

void AccerionSensor::outputUncorrectedPose(std::vector<uint8_t> data)
{
    UncorrectedPose up;

    up.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;

    up.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    up.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    up.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;

    up.xVel = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    up.yVel = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    up.thVel = ((int16_t)ntohl(*((int16_t *)&data[28]))) / 100.0;

    up.standardDeviationVelocity.x = ((uint32_t)ntohl(*((uint32_t *)&data[30]))) / 1000000.0;
    up.standardDeviationVelocity.y = ((uint32_t)ntohl(*((uint32_t *)&data[34]))) / 1000000.0;
    up.standardDeviationVelocity.theta = ((uint32_t)ntohl(*((uint32_t *)&data[38]))) / 100.0;
    up.qualityEstimate = data[42];

    if (uncorrectedPoseCallBack)
    {
        uncorrectedPoseCallBack(up);
    }
}

void AccerionSensor::subscribeToDiagnostics(_diagnosticsCallBack diagCallback)
{
    diagnosticsCallBack = diagCallback;
}

void AccerionSensor::outputDiagnostics(std::vector<uint8_t> data)
{
    Diagnostics diag;

    diag.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    diag.modes = (uint16_t)ntohs(*((uint16_t *)&data[8]));
    diag.warningCodes = (uint16_t)ntohs(*((uint16_t *)&data[10]));
    diag.errorCodes = (uint32_t)ntohl(*((uint32_t *)&data[12]));
    diag.statusCodes = data[16];

    if (diagnosticsCallBack)
    {
        diagnosticsCallBack(diag);
    }
}

void AccerionSensor::subscribeToDriftCorrections(_driftCorrectionCallBack dcCallback)
{
    driftCorrectionCallBack = dcCallback;
}

void AccerionSensor::outputDriftCorrection(std::vector<uint8_t> data)
{
    DriftCorrection dc;
    dc.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    dc.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    dc.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    dc.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    dc.xDelta = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    dc.yDelta = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    dc.thDelta = ((int32_t)ntohl(*((int32_t *)&data[28]))) / 100.0;

    dc.cumulativeTravelledDistance = ((uint32_t)ntohl(*((uint32_t *)&data[32]))) / 1000000.0;
    dc.cumulativeTravelledHeading = ((uint32_t)ntohl(*((uint32_t *)&data[36]))) / 100.0;
    dc.errorPercentage = (uint32_t)ntohl(*((uint32_t *)&data[40]));

    dc.QRID = (uint16_t)ntohs(*((uint16_t *)&data[44]));
    dc.typeOfCorrection = data[46]; //1 = infraless, 2 = qr, 3 = manual
    dc.qualityEstimate = data[47];
    if (driftCorrectionCallBack)
    {
        driftCorrectionCallBack(dc);
    }
}

void AccerionSensor::subscribeToQualityEstimates(_qualityEstimateCallBack qeCallback)
{
    qualityEstimateCallBack = qeCallback;
}

void AccerionSensor::outputQualityEstimate(std::vector<uint8_t> data)
{
    QualityEstimate qe;
    qe.qualityEstimator1 = data[0];
    qe.qualityEstimator2 = data[1];
    qe.qualityEstimator3 = data[2];
    qe.qualityEstimator4 = data[3];
    qe.qualityEstimator5 = data[4];
    qe.qualityEstimator6 = data[5];
    qe.qualityEstimator7 = data[6];
    qe.qualityEstimator8 = data[7];
    qe.qualityEstimator9 = data[8];
    qe.qualityEstimator10 = data[9];

    if (qualityEstimateCallBack)
    {
        qualityEstimateCallBack(qe);
    }
}

void AccerionSensor::subscribeToLineFollowerData(_lineFollowerCallBack lfCallback)
{
    lineFollowerCallBack = lfCallback;
}

void AccerionSensor::outputLineFollowerData(std::vector<uint8_t> data)
{
    LineFollowerData lfd;
    lfd.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    lfd.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    lfd.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    lfd.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    lfd.closestPointX = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    lfd.closestPointY = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    lfd.reserved = (int32_t)ntohl(*((int32_t *)&data[28]));
    lfd.clusterID = (uint16_t)ntohs(*((uint16_t *)&data[32]));
    if (lineFollowerCallBack)
    {
        lineFollowerCallBack(lfd);
    }
}

void AccerionSensor::subscribeToMarkerPosPacketStartStop(_acknowledgementCallBack mpCallBack)
{
    markerPosStartStopCallBack = mpCallBack;
}

void AccerionSensor::acknowledgeMarkerPosPacketStartStop(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (markerPosStartStopCallBack)
    {
        markerPosStartStopCallBack(ack);
    }
}

void AccerionSensor::subscribeToMarkerPosPacket(_markerPosPacketCallBack mppCallback)
{
    markerPosPacketCallBack = mppCallback;
}

void AccerionSensor::outputMarkerPosPacket(std::vector<uint8_t> data)
{
    MarkerPosPacket mpp;
    mpp.numberOfMarkersInMessage = (uint16_t)ntohs(*((uint16_t *)&data[0]));
    mpp.markers.clear();
    for (int i = 0; i < mpp.numberOfMarkersInMessage; i++)
    {
        Marker m;
        int baseIndex = i * 23;
        m.xPos = ((int32_t)ntohl(*((int32_t *)&data[baseIndex + 2]))) / 1000000.0;
        m.yPos = ((int32_t)ntohl(*((int32_t *)&data[baseIndex + 6]))) / 1000000.0;
        m.clusterID = (uint16_t)ntohs(*((uint16_t *)&data[baseIndex + 10]));
        m.signatureID = (uint32_t)ntohl(*((uint32_t *)&data[baseIndex + 12]));
        m.sigStatus = (uint32_t)ntohl(*((uint32_t *)&data[baseIndex + 16]));
        m.stdDevX = ((uint32_t)ntohl(*((uint32_t *)&data[baseIndex + 17]))) / 1000000.0;
        m.stdDevY = ((uint32_t)ntohl(*((uint32_t *)&data[baseIndex + 21]))) / 1000000.0;
        mpp.markers.push_back(m);
    }

    if (markerPosPacketCallBack)
    {
        markerPosPacketCallBack(mpp);
    }
}

void AccerionSensor::outputClusterIds(std::vector<uint8_t> data)
{
    uint32_t messageSize = (uint32_t)ntohl(*((uint32_t *)&data[0]));
    auto size = messageSize - (CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + CommandFields::MESSAGE_SIZE_LENGTH);

    size /= 2; // clusterIds are 2 bytes

    for (int i = 0; i < size; i++)
    {
        receivedClusterIds.push_back( ntohs(*((uint16_t *)&data[4 + 2*i])) );
    }

    clusterIdsAckCV.notify_all();

    if (clusterIdsCallBack)
    {
        clusterIdsCallBack(receivedClusterIds);
    }
}

void AccerionSensor::requestMarkerMap(_markerPosPacketCallBack mppCallback, bool requestAll, uint16_t clusterId)
{
    subscribeToMarkerPosPacket(mppCallback);
    requestMarkerMap(requestAll, clusterId);
}

void AccerionSensor::requestMarkerMap(bool requestAll, uint16_t clusterId)
{
    auto msg = CMD_GET_SIGNATURE_MARKER_MAP;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(msg, 
            RequestClusterCommand(msg, requestAll, clusterId).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setGetClusterIdsCallback(_clusterIdsCallBack clCallBack)
{
    clusterIdsCallBack = clCallBack;
}

void AccerionSensor::getClusterIds()
{
    receivedClusterIds.clear();
    const auto msg = CMD_GET_CLUSTER_IDS;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(msg, EmptyCommand(msg).serialize());
    outgoingCommandsMutex.unlock();

}

void AccerionSensor::getClusterIds(_clusterIdsCallBack clCallBack)
{
    getClusterIds();
    setGetClusterIdsCallback(clCallBack);
}

bool AccerionSensor::getClusterIdsBlocking(std::vector<uint16_t> &clusterIds)
{
    getClusterIds();

    std::unique_lock<std::mutex> lck(clusterIdsAckMutex);
    if (clusterIdsAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return false;
    }

    clusterIds.clear();
    for(const auto &id : receivedClusterIds)
    {
        clusterIds.push_back(id);
    }   

    return true;
}

void AccerionSensor::subscribeToConsoleOutputInfo(_consoleOutputCallback conCallback)
{
    consoleOutputCallBack = conCallback;
}

void AccerionSensor::outputConsoleOutputInfo(std::vector<uint8_t> data)
{
    std::string msg(data.begin() + MESSAGE_SIZE_LENGTH, data.end());

    if (consoleOutputCallBack)
    {
        consoleOutputCallBack(msg);
    }
}

void AccerionSensor::setGetMapCallbacks(_progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    getMapProgressCallBack = progressCB;
    getMapDoneCallBack = doneCB;
    getMapStatusCallBack = statusCB;
}

bool AccerionSensor::getMap(std::string destinationPath)
{
    if (!isInProgress)
    {
        isInProgress = true;
        totalMessagesToBeTransferred_ = 0;
        msgcounter = 0;
        mapSharingPath_ = destinationPath;
        filesSuccessfullyTransferred = true;
        totalFileSize_ = 0;
        totalsent = 0;
        return retrieveFirstMapPiece();
    }
    else
    {
        getMapStatusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionSensor::getMap(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    setGetMapCallbacks(progressCB, doneCB, statusCB);
    return getMap(destinationPath);
}

bool AccerionSensor::retrieveFirstMapPiece()
{

    if (!tcpClient->getConnected())
    {
        getMapStatusCallBack(CONNECTION_FAILED);
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        return false;
    }

    if (FileExists(mapSharingPath_))
    {
        if (remove(mapSharingPath_.c_str()) != 0)
        {
            getMapStatusCallBack(FAILED_TO_REMOVE_EXISTING);
            filesSuccessfullyTransferred = false;
            isInProgress = false;
            return false;
        }
    }
    getMapStatusCallBack(PACKING_MAP);
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_MAP, UINT32Command(CMD_GET_MAP, 0).serialize());
    outgoingCommandsMutex.unlock();
    return true;
}

void AccerionSensor::retrievedMapPiece(std::vector<uint8_t> receivedCommand_)
{
    //quint8 cmdType = *(reinterpret_cast<quint8*>(&receivedCommand_[4]));
    //uint32_t msgLength  = (uint32_t) ntohl(*((uint32_t*)&receivedCommand_[0]));
    if (receivedCommand_[4] == 0x00) // done, check next
    {
        isInProgress = false;
        if(mapSharingFile != NULL)
        {
            fclose(mapSharingFile);
            mapSharingFile = NULL;
        }
        getMapDoneCallBack(filesSuccessfullyTransferred, "Success");
    }
    else if (receivedCommand_[4] == 0x01) // fail, check next
    {
        isInProgress = false;
        if (totalMessagesToBeTransferred_ != 0)
        {
            if(mapSharingFile != NULL)
            {
                fclose(mapSharingFile);
                mapSharingFile = NULL;
            }
        }
        filesSuccessfullyTransferred = false;
        getMapDoneCallBack(filesSuccessfullyTransferred, "Failure");
    }
    else if (receivedCommand_[4] == 0x02) // info received
    {
        totalMessagesToBeTransferred_ = (uint32_t)ntohl(*((uint32_t *)&receivedCommand_[5]));
        mapSharingFile = fopen(mapSharingPath_.c_str(), "ab");
        if (mapSharingFile)
        {
            msgcounter++;
        }
        retrieveNextMapPiece();
    }
    else if (receivedCommand_[4] == 0x03) // pckg received
    {
        double prgrss = ((msgcounter)*100.0 / totalMessagesToBeTransferred_);
        getMapStatusCallBack(RETRIEVING_MAP);
        getMapProgressCallBack(static_cast<int>(prgrss));

        size_t bytesRead = receivedCommand_.size() - 5;

        fwrite(&receivedCommand_[5], sizeof(unsigned char), bytesRead, mapSharingFile);

        fflush(mapSharingFile);

        msgcounter++;
        retrieveNextMapPiece();
    }
}

void AccerionSensor::retrieveNextMapPiece()
{
    if (!tcpClient->getConnected())
    {
        getMapStatusCallBack(CONNECTION_FAILED);
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        return;
    }

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_MAP, UINT32Command(CMD_GET_MAP, msgcounter).serialize());
    outgoingCommandsMutex.unlock();
}

bool AccerionSensor::sendMap(std::string sourcePath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB, int strategy)
{
    if (!isInProgress)
    {
        isInProgress = true;
        mapStrategy = strategy;
        totalMessagesToBeTransferred_ = 0;
        msgcounter = 0;
        sendMapProgressCallBack = progressCB;
        sendMapDoneCallBack = doneCB;
        sendMapStatusCallBack = statusCB;
        mapSharingPath_ = sourcePath;
        return sendFirstMapPiece();
    }
    else
    {
        sendMapStatusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionSensor::sendFirstMapPiece()
{
    msgcounter = 0;
    totalsent = 0;
    totalFileSize_ = 0;
    totalMessagesToBeTransferred_ = 0;

    if (!tcpClient->getConnected())
    {
        sendMapStatusCallBack(CONNECTION_FAILED);
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        return false;
    }
    //std::cout << "path: " << mapSharingPath_ << std::endl;
    std::ifstream in(mapSharingPath_, std::ios::ate);
    in.seekg(0, std::ios::end);
    totalFileSize_ = static_cast<long long>(in.tellg());
    int fileSizeInMB = totalFileSize_ / 1024 / 1024;
    
    // Check if object is valid
    if (!in)
    {
        std::cout << "File open failure..." << std::endl;
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        sendMapStatusCallBack(FAILED_TO_OPEN_FILE);
        return false;
    }

    in.close();
    totalMessagesToBeTransferred_ = static_cast<uint32_t>(totalFileSize_ / bufferSize);
    //std::cout << "(totalFileSize_ % bufferSize)" << totalFileSize_ % bufferSize << std::endl;
    if(totalFileSize_ % bufferSize != 0)
    {
        totalMessagesToBeTransferred_ += 1;
    }

    //Sending start message
    uint32_t messageLength = 0 + std::get<1>(commandValues.find(CMD_PLACE_MAP)->second);
    //uint8_t option = 0;
    std::vector<uint8_t> empty;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_PLACE_MAP, LegacyUploadCommand(CMD_PLACE_MAP, messageLength, 0, fileSizeInMB, empty).serialize()); // first 0 is packettype = start, second is messageIndex, which is still 0 here.
    outgoingCommandsMutex.unlock();
    //socketsVector[transferCounter]->tcpSocket_.flush();

    //sending first packet message
    mapSharingFile = fopen(mapSharingPath_.c_str(), "rb");
    if (tcpClient->getConnected())
    {
        if (mapSharingFile)
        {
            fseek(mapSharingFile, 0, SEEK_SET);
            size_t bytesRead = 0;
            unsigned char buffer[bufferSize]; // array of bytes, not pointers-to-bytes
            // read up to sizeof(buffer) bytes
            bytesRead = fread(buffer, 1, sizeof(buffer), mapSharingFile);

            // process bytesRead worth of data in buffer

            auto messageLength = static_cast<uint32_t>(bytesRead + std::get<1>(commandValues.find(CMD_PLACE_MAP)->second));
            std::vector<uint8_t> dataToSend = std::vector<uint8_t>(buffer, buffer + bytesRead);
            outgoingCommandsMutex.lock();
            outgoingCommands.emplace_back(CMD_PLACE_MAP, LegacyUploadCommand(CMD_PLACE_MAP, messageLength, 1, 0, dataToSend).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
            outgoingCommandsMutex.unlock();

            totalsent += bytesRead;
            msgcounter++;
            sendMapStatusCallBack(SENDING_MAP);
            //socketsVector[transferCounter]->tcpSocket_.flush();
            std::cout << "Map piece sent.." << std::endl;
            return true;
        }
        else
        {
            std::cout << "File reading failure.." << std::endl;
            filesSuccessfullyTransferred = false;
            isInProgress = false;
            sendMapStatusCallBack(FAILED_TO_OPEN_FILE);
            return false;
        }
    }
    else
    {
        std::cout << "Connetion failure...." << std::endl;
        sendMapStatusCallBack(CONNECTION_FAILED);
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        return false;
    }
}

void AccerionSensor::retrievedMapAck(std::vector<uint8_t> receivedCommand_) // 1 = map done, 2 = failed, 3 = pckg success, 4 = package failed, 5 = encrypting, 6=  unzipping
{
    if (receivedCommand_[0] == 0x01) // update succeeded, check next
    {
        isInProgress = false;
        if(mapSharingFile != NULL)
        {
            fclose(mapSharingFile);
            mapSharingFile = NULL;
        }
        sendMapDoneCallBack(filesSuccessfullyTransferred, "Success");
    }
    else if (receivedCommand_[0] == 0x02) // update failed, check next
    {
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        if(mapSharingFile != NULL)
        {
            fclose(mapSharingFile);
            mapSharingFile = NULL;
        }
        sendMapDoneCallBack(filesSuccessfullyTransferred, "Failure");
    }
    else if (receivedCommand_[0] == 0x03 || receivedCommand_[0] == 0x04) // either send next message, current message or stop message
    {
        double prgrss = ((msgcounter)*100.0 / totalMessagesToBeTransferred_);
        std::cout << "emitting status.." << std::endl;
        sendMapStatusCallBack(SENDING_MAP);
        std::cout << "emitting progress" << std::endl;
        sendMapProgressCallBack(static_cast<int>(prgrss));
        //int msgReceived = (uint32_t) ntohl(*((uint32_t*)&receivedCommand_[1]));
        std::cout << "something else next.." << std::endl;
        if (prgrss == 100.0)
        {
            sendMapStatusCallBack(UNPACKING_AND_REPLACING_MAP);
        }

        if (static_cast<int>(msgcounter) == totalMessagesToBeTransferred_ && totalsent == totalFileSize_) // everything has been sent, so sending stop message
        {
            uint32_t messageLength = static_cast<uint32_t>(0 + std::get<1>(commandValues.find(CMD_PLACE_MAP)->second));
            uint8_t option = 2; // 2 = normal stop, which will replace
            std::vector<uint8_t> empty;
            if (mapStrategy == 1)
            {
                option = 3;
            }
            if (tcpClient->getConnected())
            {
                outgoingCommandsMutex.lock();
                outgoingCommands.emplace_back(CMD_PLACE_MAP, LegacyUploadCommand(CMD_PLACE_MAP, messageLength, option, 0, empty).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
                outgoingCommandsMutex.unlock();
            }
            else
            {
                isInProgress = false;
                filesSuccessfullyTransferred = false;
                sendMapStatusCallBack(CONNECTION_FAILED);
                return;
            }
        }
        else
        {
            //send next message..
            if (mapSharingFile)
            {
                unsigned char buffer[bufferSize]; // array of bytes, not pointers-to-bytes
                int bytesRead = fread(buffer, 1, sizeof(buffer), mapSharingFile);
                auto messageLength = static_cast<uint32_t>(bytesRead + std::get<1>(commandValues.find(CMD_PLACE_MAP)->second));
                std::vector<uint8_t> dataToSend = std::vector<uint8_t>(buffer, buffer + bytesRead);
                outgoingCommandsMutex.lock();
                outgoingCommands.emplace_back(CMD_PLACE_MAP, LegacyUploadCommand(CMD_PLACE_MAP, messageLength, 1, msgcounter, dataToSend).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
                outgoingCommandsMutex.unlock();
                totalsent += bytesRead;
                msgcounter++;
                std::cout << "Messagecounter: " << msgcounter << std::endl;
            }
        }
    }
    else if (receivedCommand_[0] == 0x05 || receivedCommand_[0] == 0x06)
    {
        sendMapStatusCallBack(UNPACKING_AND_REPLACING_MAP);
    }
    else if(receivedCommand_[0] == 0x07) // could not access storage
    {
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        if(mapSharingFile != NULL)
        {
            fclose(mapSharingFile);
            mapSharingFile = NULL;
        }
        sendMapDoneCallBack(filesSuccessfullyTransferred, "Sensor could not access storage");
    }
    else if(receivedCommand_[0] == 0x08) // not enough disk space
    {   
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        if(mapSharingFile != NULL)
        {
            fclose(mapSharingFile);
            mapSharingFile = NULL;
        }
        sendMapDoneCallBack(filesSuccessfullyTransferred, "Sensor does not have enough disk space..");
    }
}

void AccerionSensor::setSoftwareDetailsCallback(_softwareDetailsCallBack sdCallback)
{
    softwareDetailsCallBack = sdCallback;
}

void AccerionSensor::getSoftwareDetails()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SOFTWAREHASH, EmptyCommand(CMD_GET_SOFTWAREHASH).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::getSoftwareDetails(_softwareDetailsCallBack sdCallback)
{
    setSoftwareDetailsCallback(sdCallback);
    getSoftwareDetails();
}

SoftwareDetails AccerionSensor::getSoftwareDetailsBlocking()
{
    getSoftwareDetails();

    std::unique_lock<std::mutex> lck(softwareDetailsAckMutex);
    if (softwareDetailsAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        SoftwareDetails sd;
        sd.date = "";
        sd.softwareHash = "";
        return sd;
    }

    return receivedSoftwareDetails;
}

void AccerionSensor::acknowledgeSoftwareDetails(std::vector<uint8_t> data)
{
    SoftwareDetails sd;
    char hash[40];
    for (int i = 0; i < 40; i++)
    {
        hash[i] = data[i];
    }
    char date[12];
    for (int i = 0; i < 12; i++)
    {
        date[i] = data[40 + i];
    }
    sd.softwareHash = std::string(hash);
    sd.date = std::string(date);
    if(softwareDetailsCallBack)
    {
        softwareDetailsCallBack(sd);
    }
    std::unique_lock<std::mutex> lck(softwareDetailsAckMutex);
    receivedSoftwareDetails = sd;
    softwareDetailsAckCV.notify_all();
}

void AccerionSensor::subscribeToDriftCorrectionsMissed(_driftCorrectionsMissedCallBack dcmCallback)
{
    driftCorrectionsMissedCallBack = dcmCallback;
}

void AccerionSensor::outputDriftCorrectionsMissed(std::vector<uint8_t> data)
{
    int driftCorrectionsMissed = (uint16_t)ntohs(*((uint16_t *)&data[0]));

    if (driftCorrectionsMissedCallBack)
    {
        driftCorrectionsMissedCallBack(driftCorrectionsMissed);
    }
}

void AccerionSensor::subscribeToArucoMarkers(_arucoMarkerCallBack amCallback)
{
    arucoMarkerCallBack = amCallback;
}

void AccerionSensor::outputArucoMarker(std::vector<uint8_t> data)
{
    ArucoMarker am;
    am.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    am.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    am.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    am.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    am.markerID = (uint16_t)ntohl(*((uint16_t *)&data[20]));

    if (arucoMarkerCallBack)
    {
        arucoMarkerCallBack(am);
    }
}

void AccerionSensor::subscribeToMapLoaded(_mapLoadedCallBack mlCallback)
{
    mapLoadedCallBack = mlCallback;
}

void AccerionSensor::outputMapLoaded(std::vector<uint8_t> data)
{
    MapLoadingInfo mli;
    mli.success = data[0] == 1;
    mli.percentage = data[1];
    mli.message = "";
    
    if (mapLoadedCallBack)
    {
        mapLoadedCallBack(mli);
    }
}

void AccerionSensor::setArucoMarkerDetectionModeCallback(_acknowledgementCallBack tammCallback)
{
    toggleArucoMarkerModeCallBack = tammCallback;
}

void AccerionSensor::toggleArucoMarkerDetectionMode(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_ARUCO_MARKER_MODE, BooleanCommand(CMD_SET_ARUCO_MARKER_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleArucoMarkerDetectionMode(bool on, _acknowledgementCallBack tammCallback)
{
    setArucoMarkerDetectionModeCallback(tammCallback);
    toggleArucoMarkerDetectionMode(on);
}

int AccerionSensor::toggleArucoMarkerDetectionModeBlocking(bool on)
{
    toggleArucoMarkerDetectionMode(on);

    std::unique_lock<std::mutex> lck(arucoMarkerModeAckMutex);
    if (arucoMarkerModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedArucoMarkerModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeToggleArucoMarkerMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (toggleArucoMarkerModeCallBack)
    {
        toggleArucoMarkerModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(arucoMarkerModeAckMutex);
    receivedArucoMarkerModeAck = ack;
    arucoMarkerModeAckCV.notify_all();
}

void AccerionSensor::setBufferLengthCallback(_bufferLengthCallBack blCallBack)
{
    bufferLengthCallBack = blCallBack;
}

void AccerionSensor::setBufferLength(uint32_t bufferLength)
{
    bufferLength = std::ceil(bufferLength*1000000); // convert to micrometer for serialization
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_BUFFER_LENGTH, UINT32Command(CMD_SET_BUFFER_LENGTH, bufferLength).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setBufferLength(uint32_t bufferLength, _bufferLengthCallBack blCallBack)
{
    setBufferLengthCallback(blCallBack);
    setBufferLength(bufferLength);
}

int AccerionSensor::setBufferLengthBlocking(uint32_t bufferLength)
{
    setBufferLength(bufferLength);

    std::unique_lock<std::mutex> lck(bufferLengthAckMutex);
    if (bufferLengthAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedBufferLength == bufferLength)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::getBufferLength(_bufferLengthCallBack blCallBack)
{
    bufferLengthCallBack = blCallBack;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_BUFFER_LENGTH, EmptyCommand(CMD_GET_BUFFER_LENGTH).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::getBufferLengthBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_BUFFER_LENGTH, EmptyCommand(CMD_GET_BUFFER_LENGTH).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(bufferLengthAckMutex);
    if (bufferLengthAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    return receivedBufferLength;
}

void AccerionSensor::acknowledgeBufferLength(std::vector<uint8_t> data)
{
    int bufferLength = ((uint32_t)ntohl(*((uint32_t *)&data[0]))) / 1000000.0;

    if(bufferLengthCallBack)
    {
        bufferLengthCallBack(bufferLength);
    }

    std::unique_lock<std::mutex> lck(bufferLengthAckMutex);
    receivedBufferLength = bufferLength;
    bufferLengthAckCV.notify_all();
}

void AccerionSensor::setStartCancelBufferedRecoveryCallback(_bufferProgressCallBack bpCallBack)
{
    bufferProgressCallBack = bpCallBack;
}

void AccerionSensor::startBufferedRecovery(double xPos, double yPos, double radius)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_START_PROCESSING_BUFFER, BufferedRecoveryCommand(CMD_START_PROCESSING_BUFFER, xPos, yPos, radius).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::startBufferedRecovery(double xPos, double yPos, double radius, _bufferProgressCallBack bpCallBack)
{
    setStartCancelBufferedRecoveryCallback(bpCallBack);
    startBufferedRecovery(xPos,yPos, radius);
}

void AccerionSensor::cancelBufferedRecovery()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_STOP_PROCESSING_BUFFER, BooleanCommand(CMD_STOP_PROCESSING_BUFFER, false).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::cancelBufferedRecovery(_bufferProgressCallBack bpCallBack)
{
    setStartCancelBufferedRecoveryCallback(bpCallBack);
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_STOP_PROCESSING_BUFFER, BooleanCommand(CMD_STOP_PROCESSING_BUFFER, false).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::acknowledgeBufferProgress(std::vector<uint8_t> data)
{
    BufferProgress bp;
    bp.messageType  = data[0];
    bp.result       = data[1];
    bp.progress     = data[2];
    bp.percentageOfLowOverlap     = data[3];

    if(bufferProgressCallBack)
    {
        bufferProgressCallBack(bp);
    }
}

void AccerionSensor::setGetRecordingsListCallback(_recordingListCallBack recCallBack)
{
    recordingListCallBack = recCallBack;
}

void AccerionSensor::getRecordingsList()
{
    std::vector<uint8_t> vec;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 5, vec).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::getRecordingsList(_recordingListCallBack recCallBack)
{
    setGetRecordingsListCallback(recCallBack);
    getRecordingsList();
}

bool AccerionSensor::getRecordingsListBlocking(std::vector<std::string> &vector)
{
    getRecordingsList();

    std::unique_lock<std::mutex> lck(recordingListAckMutex);
    if (recordingListAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return false;
    }

    for(const auto & i : receivedRecordingList)
    {
        vector.push_back(i);
    }   

    return true;
}

void AccerionSensor::setDeleteRecordingsCallback(_deleteRecordingsCallBack cb)
{
    deleteRecordingsCallBack = cb;
}

void AccerionSensor::deleteRecordings(std::vector<uint8_t> indexes)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 6, indexes).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::deleteRecordings(std::vector<uint8_t> indexes, _deleteRecordingsCallBack cb)
{
    setDeleteRecordingsCallback(cb);
    deleteRecordings(indexes);
}

DeleteRecordingsResult AccerionSensor::deleteRecordingsBlocking(std::vector<uint8_t> indexes)
{
    deleteRecordings(indexes);

    std::unique_lock<std::mutex> lck(deleteRecordingsAckMutex);
    if (deleteRecordingsAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        DeleteRecordingsResult deleteRecordingsResult;
        deleteRecordingsResult.success = false;
        return deleteRecordingsResult;
    }

    return deleteRecordingsResult;
}

void AccerionSensor::setGetRecordingsCallbacks(_progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    recordingsProgressCallBack = progressCB;
    recordingsDoneCallBack = doneCB;
    recordingsStatusCallBack = statusCB;
}

bool AccerionSensor::getRecordings(std::vector<uint8_t> indexes, std::string destinationPath)
{
    if (!recordingsIsInProgress)
    {
        recordingsIsInProgress = true;
        totalRecordingsMessagesToBeTransferred_ = 0;
        recordingsMsgcounter = 0;
        recordingsPath_ = destinationPath;
        recordingIndexes_ = indexes;
        return retrieveFirstRecordingsPiece();
    }
    else
    {
        if(recordingsStatusCallBack) recordingsStatusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionSensor::getRecordings(std::vector<uint8_t> indexes, std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    setGetRecordingsCallbacks(progressCB, doneCB, statusCB);
    return getRecordings(indexes, destinationPath);
}

bool AccerionSensor::retrieveFirstRecordingsPiece()
{
    if(recordingsStatusCallBack) recordingsStatusCallBack(PACKING_RECORDINGS);

    if (!tcpClient->getConnected())
    {
        if(recordingsStatusCallBack) recordingsStatusCallBack(CONNECTION_FAILED);
        recordingsSuccessfullyTransferred = false;
        recordingsIsInProgress = false;
        return false;
    }

    if (FileExists(recordingsPath_))
    {
        if (remove(recordingsPath_.c_str()) != 0)
        {
            if(recordingsStatusCallBack) recordingsStatusCallBack(FAILED_TO_REMOVE_EXISTING);
            recordingsSuccessfullyTransferred = false;
            recordingsIsInProgress = false;
            return false;
        }
    }
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 2, recordingIndexes_).serialize());
    outgoingCommandsMutex.unlock();
    return true;
}

void AccerionSensor::retrieveNextRecordingsPiece()
{
    if (!tcpClient->getConnected())
    {
        if(recordingsStatusCallBack) recordingsStatusCallBack(CONNECTION_FAILED);
        recordingsSuccessfullyTransferred = false;
        recordingsIsInProgress = false;
        return;
    }
    uint8_t array[4];
    Serialization::serializeUInt32(recordingsMsgcounter,array,false);
    std::vector<uint8_t> vec;
    for(unsigned char i : array)
    {
        vec.push_back(i);
    }
    
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 3, vec).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::acknowledgeRecordingMsg(std::vector<uint8_t> data)
{
    int size = data.size() - 5;
    int msgType = data[4];

    if (msgType == 0x00) // done, check next
    {
        recordingsIsInProgress = false;
        if(recordingsFile != NULL)
        {
            fclose(recordingsFile);
            recordingsFile = NULL;
        }
        if(recordingsDoneCallBack) recordingsDoneCallBack(recordingsSuccessfullyTransferred, "Success");
    }
    else if (msgType == 0x01) // fail, check next
    {
        recordingsIsInProgress = false;
        if (totalRecordingsMessagesToBeTransferred_ != 0)
        {
            if(recordingsFile != NULL)
            {
                fclose(recordingsFile);
                recordingsFile = NULL;
            }
        }
        recordingsSuccessfullyTransferred = false;
        if(recordingsDoneCallBack) recordingsDoneCallBack(recordingsSuccessfullyTransferred, "Failure");
    }
    else if (msgType == 0x02) // info received
    {
        totalRecordingsMessagesToBeTransferred_ = (uint32_t)ntohl(*((uint32_t *)&receivedCommand_[5]));
        recordingsFile = fopen(recordingsPath_.c_str(), "ab");
        if (recordingsFile)
        {
            recordingsMsgcounter++;
        }
        retrieveNextRecordingsPiece();
    }
    else if (msgType == 0x03) // pckg received
    {
        double prgrss = ((recordingsMsgcounter)*100.0 / totalRecordingsMessagesToBeTransferred_);
        if(recordingsStatusCallBack) recordingsStatusCallBack(RETRIEVING_RECORDINGS);
        if(recordingsProgressCallBack) recordingsProgressCallBack(static_cast<int>(prgrss));

        size_t bytesRead = data.size() - 5;

        fwrite(&receivedCommand_[5], sizeof(unsigned char), bytesRead, recordingsFile);

        fflush(recordingsFile);

        recordingsMsgcounter++;
        retrieveNextRecordingsPiece();
    }
    else if (msgType == 0x04) // error
    {

    }
    else if (msgType == 0x05) // list
    {
        receivedRecordingList.clear();
        std::string str(receivedCommand_.begin()+5, receivedCommand_.end());

        std::istringstream f(str);
        std::string s;    
        while (getline(f, s, '|')) {
            receivedRecordingList.push_back(s);
        }

        if(recordingListCallBack)
        {
            recordingListCallBack(receivedRecordingList);
        }

        std::unique_lock<std::mutex> lck(recordingListAckMutex);
        recordingListAckCV.notify_all();
    }
    else if (msgType == 0x06) // delete
    {
        deleteRecordingsResult.failedIndexes.clear();
        if(size > 0)
        {
            for(int i = 5; i < size; i++)
            {
                deleteRecordingsResult.failedIndexes.push_back(data[i]);
            }
            deleteRecordingsResult.success = false;
        }
        else
        {
            deleteRecordingsResult.success = true;
        }

        if(deleteRecordingsCallBack)
        {
            deleteRecordingsCallBack(deleteRecordingsResult);
        }

        std::unique_lock<std::mutex> lck(deleteRecordingsAckMutex);
        deleteRecordingsAckCV.notify_all();
    }
}

std::vector<uint8_t>& AccerionSensor::captureFrame(uint8_t camIdx, std::string key)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_CAPTURE_FRAME, CaptureFrameCommand(CMD_CAPTURE_FRAME, camIdx, key).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(captureFrameAckMutex);
    if (captureFrameAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        captureFrameResult.clear();
        return captureFrameResult;
    }

    return captureFrameResult;
}

void AccerionSensor::acknowledgeFrameCaptureMsg(std::vector<uint8_t> data)
{
    captureFrameResult.clear();
    int size = data.size() - 5;
    int msgType = data[4];

    size_t bytesRead = data.size() - 5;

    for (int i = 5; i < data.size(); i++)
    {
        captureFrameResult.push_back(*((unsigned char *)&data[i]));
    }

    std::unique_lock<std::mutex> lck(captureFrameAckMutex);
    captureFrameAckCV.notify_all();
}

bool AccerionSensor::getG2OFile(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _fileTransferStatusCallBack errorCB)
{
    //register callbacks
    g2oExportTransferHelper_.registerCallBacks(progressCB, doneCB, errorCB);
    outgoingCommandsMutex.lock();
    bool result = g2oExportTransferHelper_.startDownload(TransferFileType::G2O_FILE, destinationPath, outgoingCommands);
    outgoingCommandsMutex.unlock();
    return  result;// make sure to properly forward the acknowledgement to the helper
}

void AccerionSensor::toggleSearchForLoopClosures(bool on, _progressCallBack progressCB, _doneCallBack doneCB)
{
    LCprogressCallBack  = progressCB;
    LCdoneCallBack      = doneCB;

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_FIND_LOOPCLOSURES_G2O, BooleanCommand(CMD_FIND_LOOPCLOSURES_G2O, on).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::acknowledgeToggleLoopClosureMsg(std::vector<uint8_t> data)
{
    if(LCdoneCallBack)
    {
        LCdoneCallBack(data[0],"");
    }
}

void AccerionSensor::acknowledgeGenericProgress(std::vector<uint8_t> data)
{
    switch (data[0])
    {
        case ProgressMessageType::SEARCHING_LOOP_CLOSURES:
            if(LCprogressCallBack) LCprogressCallBack(data[1]);
        break;
        case ProgressMessageType::IMPORT_G2O:
            if(g2oImportProgressCallBack_)  g2oImportProgressCallBack_(data[1]);
        break;
    }
}

bool AccerionSensor::sendG2OFile(std::string sourcePath, _progressCallBack progressCB, _doneCallBack doneCB, _fileTransferStatusCallBack errorCB)
{
    //register callbacks
    g2oUploadTransferHelper_.registerCallBacks(progressCB, doneCB, errorCB);
    outgoingCommandsMutex.lock();
    bool result = g2oUploadTransferHelper_.startUpload(TransferFileType::G2O_FILE, sourcePath, outgoingCommands);
    outgoingCommandsMutex.unlock();
    return  result;// make sure to properly forward the acknowledgement to the helper
}

void AccerionSensor::importG2OFile(_progressCallBack progressCB, _doneCallBack doneCB)
{
    g2oImportProgressCallBack_  = progressCB;
    g2oImportDoneCallBack_      = doneCB;

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REPLACE_CLUSTER_G2O, BooleanCommand(CMD_REPLACE_CLUSTER_G2O, true).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::acknowledgeImportG2ODone(std::vector<uint8_t> data)
{
    if(g2oImportDoneCallBack_)  g2oImportDoneCallBack_(data[0],"");
}

void AccerionSensor::setToggleInternalTrackingModeCallback(_acknowledgementCallBack ackCB)
{
    InternalTrackingCallBack = ackCB;
}

void AccerionSensor::toggleInternalTrackingMode(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_INTERNAL_TRACKING_MODE, BooleanCommand(CMD_SET_INTERNAL_TRACKING_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::toggleInternalTrackingMode(bool on, _acknowledgementCallBack ackCB)
{
    setToggleInternalTrackingModeCallback(ackCB);
    toggleInternalTrackingMode(on);
}

int AccerionSensor::toggleInternalTrackingModeBlocking(bool on)
{
    toggleInternalTrackingMode(on);
    std::unique_lock<std::mutex> lck(toggleInternalTrackingAckMutex);
    if (toggleInternalTrackingAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (toggleInternalTrackingAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeInternalTrackingToggle(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (InternalTrackingCallBack)
    {
        InternalTrackingCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(toggleInternalTrackingAckMutex);
    toggleInternalTrackingAck = ack;
    toggleInternalTrackingAckCV.notify_all();
}
