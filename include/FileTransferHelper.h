/* Copyright (C) 2019 Accerion / Unconstrained Robotics B.V. - All Rights Reserved
 * You( excluding Accerion employees) may NOT use, distribute and modify this code 
 * under any terms or the terms of any license, unless with full prior permission by 
 * Accerion supported by a contract signed by both parties.
 *
 * Accerion employees may use, distribute (only in compiled and encrypted form) and 
 * modify this code as long as it is intended for Accerion usage and in no way creates
 * a situation that in any way could affect Accerion negatively. Contact your supervisor
 * in case of doubt.
 *
 * For any questions contact Accerion
 * Author(s):
 *	-Accerion
 */
#ifndef FILETRANSFERHELPER_H_
#define FILETRANSFERHELPER_H_

#include <vector>
#include <functional>
#include <mutex>
#include <fstream>
#include "callbacks.h"
#include "commands.h"
#include "structs.h"
#include "APISysUtils.h"

#ifdef linux 
    #include <arpa/inet.h>
#elif _WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

struct FileTransferDetails
{
    FILE* filePtr_;
    std::string filePath_;
    bool fileIsOpen_;
    long long totalFileSize_;
    long long totalMessagesToBeSent_;
    long long messageCounter_;
    long long totalBytesSent_;
    static const int fileBufferSize_ = 1000000;
    unsigned char buffer_[fileBufferSize_];
    TransferFileType type_;

    FileTransferDetails()
    {
        reset();
    }

    void reset()
    {
        filePath_               = "";
        fileIsOpen_             = false;
        totalFileSize_          = 0;
        totalMessagesToBeSent_  = 0;
        messageCounter_         = 0;
        totalBytesSent_         = 0;
    };
};

/** 
 * @brief       FileTransferHelper is a class intended to transfer files
 */
class FileTransferHelper
{
public:
    FileTransferHelper();
    ~FileTransferHelper();

    void registerCallBacks(_progressCallBack progressCB, _doneCallBack doneCB, _fileTransferStatusCallBack errorCB);

    bool startDownload(TransferFileType type, std::string destinationpath, std::vector<Command> &outgoingCommands);
    bool startUpload(TransferFileType type, std::string sourcepath, std::vector<Command> &outgoingCommands);
    
    void incomingFileAck(std::vector<uint8_t> receivedCommand_, std::vector<Command> &outgoingCommands);
    void outgoingFileAck(std::vector<uint8_t> receivedCommand_, std::vector<Command> &outgoingCommands);
    

private:
    bool retrieveFileInfo(std::vector<Command> &outgoingCommands);
    void retrieveNextFilePiece(std::vector<Command> &outgoingCommands);

    bool sendStartUpload(std::vector<Command> &outgoingCommands);
    bool sendFilePiece(std::vector<Command> &outgoingCommands);

    _progressCallBack progressCB_;
    _doneCallBack doneCB_;
    _fileTransferStatusCallBack statusCB_;
    FileTransferDetails fileTransferDetails_;
    bool debugMessages_ = false;
};

#endif