#include "getLogs.h"

getLogsManager::getLogsManager(Address sensorIP, std::string folderLocation)
{
    updateManager = AccerionUpdateServiceManager::getInstance();
    folderLocation_ = folderLocation;
    Address localIP;
    
    while(!myUS)
    {
        std::cout << "Looking for updateService.." << std::endl;
        myUS = updateManager->getAccerionUpdateServiceByIP(sensorIP, localIP);
    }
    std::cout << "Found updateservice!" << std::endl;

}

std::string getLogsManager::getLogFileName()
{
    SerialNumber serial = myUS->getSerialNumber();
    std::stringstream filename;
    filename << folderLocation_;
    filename << std::to_string(serial.serialNumber);

    std::time_t ttime = std::time(0);
    const std::tm *local_time = std::localtime(&ttime);
    filename << "-" << std::put_time(local_time, "%d-%m-%Y-%H-%M") << ".zip";
    return filename.str();
}

bool getLogsManager::getLogs() {
    std::string destinationPath = getLogFileName();
    std::cout << "Trying to retrieve logs: " << destinationPath << std::endl;

    bool success = false;
    
    auto progress_callback = std::bind(&getLogsManager::progressCb, this, std::placeholders::_1);
    auto done_callback = std::bind(&getLogsManager::doneCb, this, std::placeholders::_1, std::ref(success));
    auto status_callback = std::bind(&getLogsManager::statusCb, this, std::placeholders::_1);

    myUS->getLogs(destinationPath, progress_callback, done_callback, status_callback);

    while(!success) {
        sleep(1);
    }
    return success;
}

void getLogsManager::progressCb(int progress) {
    std::cout << "Progress: " << progress << std::endl;
}

void getLogsManager::doneCb(bool done, bool &success) {
    if (done) {
        success = true;
        std::cout << "Log transfer successful" << std::endl;
    }
    else {
        success = false;
        std::cout << "Log transfer unsuccessful" << std::endl;
    }
}

void getLogsManager::statusCb(FileSenderStatus status) {
    std::cout << "FileSenderStatus: " << status << std::endl;
}