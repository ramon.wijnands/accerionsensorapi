#include <Jupiter.h>
#include <TritonInterface.h>
#include <algorithm>

class Mapping : public Jupiter, TritonInterface {

public:

    Mapping(Address jupiterIp, Address tritonIp, bool debug=false):
            Jupiter(jupiterIp, debug), TritonInterface(tritonIp, debug) {

        std::cout << "Enter clusterId to start mapping: ";
        std::cin >> currentClusterID_;

        Jupiter::bindUncorrectedPoseCb();
        Jupiter::toggleAbsoluteMode(false);

        Triton::bindDriftCorrectionsCb();
        Triton::clearClusterLibrary();
    }

    void sendPoseAndCovariance(Pose pose, StandardDeviation stdev, uint64_t timeOffsetInUsec) override {

        InputPose inputPose;
        inputPose.pose = pose;
        inputPose.standardDeviation = stdev;
        inputPose.timeOffsetInUsec = timeOffsetInUsec;

        Triton::sensor_->setPoseAndCovariance(inputPose);
    }

    void processDriftCorrection(Pose poseStruct) override {

        Jupiter::sensor_->setSensorPose(poseStruct, nullptr);
    }

    void startMappingMode() {
        std::cout << "Starting Mapping with clusterId: " << currentClusterID_ << std::endl;
        Triton::toggleMapping(true, currentClusterID_);
    }

    void stopMappingMode() {
        Triton::toggleMapping(false, currentClusterID_);
    }
};

Address parseIp(std::stringstream ipArg) {

    std::string substr;
    std::vector<uint8_t> ip;

    while (getline(ipArg, substr, '.')) {
        ip.push_back(std::stoi(substr));
    }

    return Address {ip[0], ip[1], ip[2], ip[3]};

}

int main(int argc, char** argv) {

    if(argc!=3){
        std::printf("Please include Jupiter and Triton Ip addresses; e.g. ./... 10.42.0.116 10.42.0.143 \n");
        return 1;
    }

    Address jupiterIp = parseIp(std::stringstream(argv[2]));
    Address tritonIp = parseIp(std::stringstream(argv[1]));

    Mapping *accerionSystem;

    // Try to connect Jupiter and Triton sensors
    try{
        accerionSystem = new Mapping(jupiterIp, tritonIp, true);
    }
    catch (const std::exception& ex ){
        std::printf("Exception: %s \n", ex.what());
        return 0;
    }

    accerionSystem->startMappingMode();

    std::cout << "Finish mapping by pressing 'enter' key..." << std::endl;

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    do {
    } while(std::cin.get()!='\n');

    accerionSystem->stopMappingMode();
}