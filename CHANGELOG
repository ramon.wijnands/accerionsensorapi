#################################################
#                                               #
#	   Accerion Sensor API Changelog   	        #
#                                               #
#          LICENSED UNDER APACHE 2.0            #
#################################################

# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [5.1.3]
### Added
- Multiple variants of existing methods to improve integrations
- Method to get clusterID's(Not available yet..)
### Changed
- Renamed timestamp in inputpose to timeOffsetInUsec
- Removed bounding box for internal tracking functionality
- Implementation of getting map marker messages
- Limited Relative Tracking mode renamed to InternalTracking
- API parts of Buffered Recovery Mode(Mode not available yet..)
### Deprecated
- GetAllSensors method
### Removed
-
### Fixed
- ClusterID messages not received on TCP
### Security


## [pre 5.1.0]
### Added
- Added multiple methods to set callbacks before actually setting modes/values etc.
- Added limited relative tracking mode, used in floor qualifier and demo modes.
### Changed
- SensorManager will now only return sensors in list that have been active in the past 2 seconds
- UpdateServiceManager will now only return sensors in list that have been active in the past 10 seconds
### Deprecated
-
### Removed
- 
### Fixed
- 
### Security

## [pre 5.1.0]
### Added
- G2O file transfer, import, export
### Changed
- 
### Deprecated
### Removed
- Old QR Functionality
### Fixed
- 
### Security

## [pre 4.0.5]
### Added
- All messages to sensor
- UDP
- TCP
- Copyright notices
- More documentation
- Marker Pos Start/Stop message
- Drift Corrections Missed message
- Aruco Marker Mode messages
- UpdateServiceManager and UpdateService to interact with UpdateService of sensors
- Messages related to Buffered Recovery Mode(interface only, backend not implemented yet)
- New way of retrieving sensors without having to wait manually for its appearance
### Changed
- The way compiling with PIC flag can be enabled; now you set ACCERION_PIC_COMPILE
- Refactored methods, in particular the callback typedefs
- Improved some method signatures and the way communication is setup
- Renamed and restructured several structs
- Renamed reset to reboot
- Moved and restructured some minor parts
- In/outputs are now meters and degrees
- Min. version of CMake
### Deprecated
### Removed
- Unused variables
### Fixed
- AddQR functionality was using wrong command structure
- Fixed inputs of PoseCommand and PoseAndCovarianceCommand
- Fixed Get Serial Number
- Fixed the way sensorManager compared detected sensors(previously ip only, now also looks for serial number)
### Security
