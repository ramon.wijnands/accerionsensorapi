# Triton with Jupiter as external reference (Triton + Jupiter)

This example is based on using the 'simple-Triton-interface. Follow the steps below to execute either the 'Mapping' or 'AbsLocalization' script. 

<code> $ cd Triton-with-Jupiter-ext-ref </code> <br>
<code> $ mkdir build </code> <br>
<code> $ cd build </code> <br>
<code> $ cmake .. </code> <br>
<code> $ make </code> <br>

## Mapping
To start mapping execute the <code>Mapping</code> command followed by the ip address of your Jupiter and Triton respectively:

<code>e.g. $ ./Mapping 192.168.178.100 192.168.178.199</code> <br>

Now follow the instructions in your command-line-interface by entering a cluster Id and start mapping.

At any time press 'enter' to stop mapping and store the current cluster on your sensor.

## Localization
To start absolute localization first make sure to load a map. Then execute the <code>AbsLocalization</code> command followed by the ip address of your Jupiter and Triton respectively:

<code>e.g. $ ./AbsLocalization 192.168.178.100 192.168.178.199</code> <br>


