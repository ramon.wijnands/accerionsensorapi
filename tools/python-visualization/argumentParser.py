"""
Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""

import argparse

# cli arguments
list_of_sensor_choices = ['Triton', 'Pluto', 'Jupiter']

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", required=False, default="./", help="Please specify valid path to sensor log directory")
parser.add_argument("-s", "--sensor", required=False, default="Triton",
                    choices=list_of_sensor_choices, help="specify the sensor type")

args = vars(parser.parse_args())