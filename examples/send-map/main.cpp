#include <iostream>
#include "sendMap.h"
#include <sstream>
#include <iomanip>
#include <string>

Address parseIp(std::stringstream ipArg) {

    std::string substr;
    std::vector<uint8_t> ip;

    while (getline(ipArg, substr, '.')) {
        ip.push_back(std::stoi(substr));
    }

    return Address {ip[0], ip[1], ip[2], ip[3]};

}

int main(int argc, char *argv[]){
    if(argc!=4){
        std::printf("Please include sensor Ip address, map location (i.e. full path to .amf file) and strategy (0: replace, 1: merge); e.g. ./sendMap 192.168.0.10 /home/$USER/598100136-28-09-2020-14-28.amf 0\n");
        return 1;
    }
    Address sensorIp = parseIp(std::stringstream(argv[1]));
    std::string folderLocation = std::string(argv[2]);
    int strategy = atoi(argv[3]); // strategy int value 0 = replace 1 = merge
    sendMapManager *mngr = new sendMapManager(sensorIp, folderLocation, strategy);
    sleep(2);
    
    mngr->sendMap();
    sleep(2);
        
    return 0;
}