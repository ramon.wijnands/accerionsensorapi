# Examples to use with Accerion Sensor C++ API

| Example                    	| Description                                           	| Sensors          	|
|-----------------------------	|-------------------------------------------------------	|------------------	|
| get-backup-logs              	| Script to retrieve backup logs from sensor                   	| Triton, Jupiter  	|
| get-logs                    	| Script to retrieve logs from sensor                   	| Triton, Jupiter  	|
| get-map                    	| Script to retrieve the current map                   	| Triton, Jupiter  	|
| sample-manager              	| Example on how to setup a sensor manager            	| Triton, Jupiter  	|
| send-map                    	| Script to transfer a map file (.amf) to a sensor                   	| Triton, Jupiter  	|
| simple-Triton-interface     	| Minimal interface on how to integrate a Triton sensor 	| Triton           	|
| Triton-with-Jupiter-ext-ref 	| Triton with Jupiter as external reference, based on the simple-Triton-interface             	| Triton + Jupiter 	|