# Script to retrieve backup logs from sensor (Triton, Jupiter)

Make sure to change the ip address within 'main.cpp' to match your sensor. Then follow the steps below: 

<code> $ cd get-logs </code> <br>
<code> $ mkdir build </code> <br>
<code> $ cd build </code> <br>
<code> $ cmake .. </code> <br>
<code> $ make </code> <br>

Now execute the commmand below attached by your sensor ip address and folder to store the logs. 

<code> $ ./getLogs 192.168.178.100 "log_directory" </code> <br>

