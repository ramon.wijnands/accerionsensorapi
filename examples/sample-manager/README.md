# Example on how to setup a sensor manager (Triton, Jupiter)

Make sure to change the ip address within 'main.cpp' to match your sensor. Then follow the steps below: 

<code> $ cd sample-manager </code> <br>
<code> $ mkdir build </code> <br>
<code> $ cd build </code> <br>
<code> $ cmake .. </code> <br>
<code> $ make </code> <br>
<code> $ ./sampleManager </code> <br>

