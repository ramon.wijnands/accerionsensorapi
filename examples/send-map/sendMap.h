#include <AccerionSensorAPI/AccerionSensorAPI.h>

class sendMapManager
{
 public:
 sendMapManager(Address sensorIP, std::string folderLocation, int strategy);
 ~sendMapManager();
 bool sendMap();
 std::string sendMapFileName();
 std::string folderLocation_;
 int strategy_;
 
 private:
 AccerionSensorManager* sensorManager;
 AccerionSensor* mySensor = NULL;
 void statusCb(FileSenderStatus status);
 void doneCb(bool done, bool &success);
 void progressCb(int progress);
 
};

