/* Copyright (C) 2019 Accerion / Unconstrained Robotics B.V. - All Rights Reserved
 * You( excluding Accerion employees) may NOT use, distribute and modify this code 
 * under any terms or the terms of any license, unless with full prior permission by 
 * Accerion supported by a contract signed by both parties.
 *
 * Accerion employees may use, distribute (only in compiled and encrypted form) and 
 * modify this code as long as it is intended for Accerion usage and in no way creates
 * a situation that in any way could affect Accerion negatively. Contact your supervisor
 * in case of doubt.
 *
 * For any questions contact Accerion
 * Author(s):
 *	-Accerion
 */

#include "FileTransferHelper.h"

FileTransferHelper::FileTransferHelper()
{
#ifdef DEBUG
    debugMessages_ = true;
#endif
}

FileTransferHelper::~FileTransferHelper()
{

}

bool FileTransferHelper::retrieveFileInfo(std::vector<Command> &outgoingCommands)
{
    if (APISysUtils::fileExists(fileTransferDetails_.filePath_))
    {
        if (remove(fileTransferDetails_.filePath_.c_str()) != 0)
        {
            if(statusCB_) statusCB_(FileTransferStatus::FILE_REMOVAL_FAILURE);
            return false;
        }
    }
    if(statusCB_) statusCB_(FileTransferStatus::FILE_GATHERING_INFO);

    outgoingCommands.emplace_back(CommandIDs::CMD_GET_FILE, GetFileCommand(CommandIDs::CMD_GET_FILE, fileTransferDetails_.type_, 0).serialize());
    return true;
}

void FileTransferHelper::incomingFileAck(std::vector<uint8_t> receivedCommand_, std::vector<Command> &outgoingCommands)
{
    switch(receivedCommand_[5])
    {
        case MessageType::INFO:
            {

                if (debugMessages_) std::cout << "RECEIVED INFO " << std::endl;
                fileTransferDetails_.totalMessagesToBeSent_ = (uint32_t)ntohl(*((uint32_t *)&receivedCommand_[6]));
                fileTransferDetails_.filePtr_ = fopen(fileTransferDetails_.filePath_.c_str(), "ab");
                if (fileTransferDetails_.filePtr_)
                {
                    fileTransferDetails_.messageCounter_++;
                }
                if(statusCB_) statusCB_(FileTransferStatus::FILE_TRANSFER_IN_PROGRESS);
                retrieveNextFilePiece(outgoingCommands);
            }
        break;
        case MessageType::PACKAGE:
            {
                if (debugMessages_) std::cout << "RECEIVED PACKAGE" << std::endl;
                double prgrss = ((fileTransferDetails_.messageCounter_)*100.0 / fileTransferDetails_.totalMessagesToBeSent_);
                if(progressCB_) progressCB_(static_cast<int>(prgrss));

                size_t bytesRead = receivedCommand_.size() - 6;// 6 -> 4 bytes for messagelength, 1 for filetype, 1 for messagetype

                fwrite(&receivedCommand_[6], sizeof(unsigned char), bytesRead, fileTransferDetails_.filePtr_);
                
                fflush(fileTransferDetails_.filePtr_);

                fileTransferDetails_.messageCounter_++;
                retrieveNextFilePiece(outgoingCommands);
            }
        break;
        case MessageType::DONE:
            {
                if (debugMessages_) std::cout << "FILE TRANSFER DONE" << std::endl;
                if(fileTransferDetails_.filePtr_ != NULL)
                {
                    fclose(fileTransferDetails_.filePtr_);
                    fileTransferDetails_.filePtr_ = NULL;
                }
                if(doneCB_) doneCB_(true, "Success");
            }
        break;
        case MessageType::FAIL:
            {
                if (debugMessages_) std::cout << "TRANSFER FAILURE" << std::endl;
                if (fileTransferDetails_.totalMessagesToBeSent_ != 0)
                {
                    if(fileTransferDetails_.filePtr_ != NULL)
                    {
                        fclose(fileTransferDetails_.filePtr_);
                        fileTransferDetails_.filePtr_ = NULL;
                    }
                }
                if(doneCB_) doneCB_(false, "Failure");

            }
        break;
    }
}

void FileTransferHelper::retrieveNextFilePiece(std::vector<Command> &outgoingCommands)
{
    outgoingCommands.emplace_back(CommandIDs::CMD_GET_FILE, GetFileCommand(CommandIDs::CMD_GET_FILE, fileTransferDetails_.type_, fileTransferDetails_.messageCounter_).serialize());
}

void FileTransferHelper::registerCallBacks(_progressCallBack progressCB, _doneCallBack doneCB, _fileTransferStatusCallBack statusCB)
{
    progressCB_ = progressCB;
    doneCB_     = doneCB;
    statusCB_   = statusCB;
}

bool FileTransferHelper::startDownload(TransferFileType type, std::string destinationPath, std::vector<Command> &outgoingCommands)
{
    fileTransferDetails_.reset();
    fileTransferDetails_.type_ = type;
    fileTransferDetails_.filePath_ = destinationPath;
    return retrieveFileInfo(outgoingCommands);
}

bool FileTransferHelper::startUpload(TransferFileType type, std::string sourcePath, std::vector<Command> &outgoingCommands)
{
    fileTransferDetails_.reset();
    fileTransferDetails_.type_ = type;
    fileTransferDetails_.filePath_ = sourcePath;
    return sendStartUpload(outgoingCommands);
}

bool FileTransferHelper::sendStartUpload(std::vector<Command> &outgoingCommands)
{
    std::ifstream in(fileTransferDetails_.filePath_, std::ios::ate);
    in.seekg(0, std::ios::end);
    fileTransferDetails_.totalFileSize_ = static_cast<long long>(in.tellg());

    // Check if object is valid
    if(!in)
    {
        if(statusCB_) statusCB_(FileTransferStatus::FILE_READ_FAILURE);
        return false;
    }
    in.close();

    fileTransferDetails_.totalMessagesToBeSent_ = static_cast<long long>(ceil(static_cast<double>(fileTransferDetails_.totalFileSize_)/fileTransferDetails_.fileBufferSize_));
    std::vector<uint8_t> contents;
    outgoingCommands.emplace_back(CommandIDs::CMD_UPLOAD_FILE, UploadFileCommand(CommandIDs::CMD_UPLOAD_FILE, fileTransferDetails_.type_, MessageType::INFO, 0, contents).serialize());
    fileTransferDetails_.filePtr_ = fopen(fileTransferDetails_.filePath_.c_str(), "rb");
    if(fileTransferDetails_.filePtr_)
    {
        fseek(fileTransferDetails_.filePtr_, 0, SEEK_SET);
    }
    return true;
}

bool FileTransferHelper::sendFilePiece(std::vector<Command> &outgoingCommands)
{
    std::cout << "Sending piece.." << std::endl;
    if (fileTransferDetails_.filePtr_)
    {
        int bytesRead = fread(fileTransferDetails_.buffer_, 1, sizeof(fileTransferDetails_.buffer_), fileTransferDetails_.filePtr_);
        fileTransferDetails_.totalBytesSent_ += bytesRead;
        std::vector<uint8_t> contents;
        for(int i = 0; i < bytesRead; i++)
        {
            contents.push_back(fileTransferDetails_.buffer_[i]);
        }
        outgoingCommands.emplace_back(CommandIDs::CMD_UPLOAD_FILE, UploadFileCommand(CommandIDs::CMD_UPLOAD_FILE, fileTransferDetails_.type_, MessageType::PACKAGE, fileTransferDetails_.messageCounter_, contents).serialize());

        fileTransferDetails_.messageCounter_++;
    }
    else
    {
        if(statusCB_) statusCB_(FileTransferStatus::FILE_READ_FAILURE);
        return false;
    }
    
}

void FileTransferHelper::outgoingFileAck(std::vector<uint8_t> receivedCommand_, std::vector<Command> &outgoingCommands)
{
    switch(receivedCommand_[1])
    {
        case MessageType::INFO:
            {
                std::cout << "Received info.." << std::endl;
                sendFilePiece(outgoingCommands);
            }
        break;
        case MessageType::PACKAGE:
            {                
                if(progressCB_) progressCB_(fileTransferDetails_.totalBytesSent_ * 100 / fileTransferDetails_.totalFileSize_);
                if(ntohl(*((uint32_t*)&receivedCommand_[2])) == fileTransferDetails_.totalMessagesToBeSent_-1 && fileTransferDetails_.totalFileSize_ == fileTransferDetails_.totalBytesSent_)
                {
                    //done
                    std::vector<uint8_t> contents;
                    contents.push_back(0);
                    outgoingCommands.emplace_back(CommandIDs::CMD_UPLOAD_FILE, UploadFileCommand(CommandIDs::CMD_UPLOAD_FILE, fileTransferDetails_.type_, MessageType::DONE, fileTransferDetails_.messageCounter_, contents).serialize());
                }
                else
                {
                    sendFilePiece(outgoingCommands);
                }
            }
        break;
        case MessageType::DONE:
            {
                if (debugMessages_) std::cout << "FILE TRANSFER DONE" << std::endl;
                if(fileTransferDetails_.filePtr_ != NULL)
                {
                    fclose(fileTransferDetails_.filePtr_);
                    fileTransferDetails_.filePtr_ = NULL;
                }
                if(doneCB_) doneCB_(true, "Success");
            }
        break;
        case MessageType::FAIL:
            {
                if (debugMessages_) std::cout << "TRANSFER FAILURE" << std::endl;
                if (fileTransferDetails_.totalMessagesToBeSent_ != 0)
                {
                    if(fileTransferDetails_.filePtr_ != NULL)
                    {
                        fclose(fileTransferDetails_.filePtr_);
                        fileTransferDetails_.filePtr_ = NULL;
                    }
                }
                if(doneCB_) doneCB_(false, "Failure");

            }
        break;
    }
}