#include <AccerionSensorAPI/AccerionSensorAPI.h>

class getMapManager
{
 public:
 getMapManager(Address sensorIP, std::string folderLocation);
 ~getMapManager();
 bool getMap();
 std::string getMapFileName();
 std::string folderLocation_;
 
 private:
 AccerionSensorManager* sensorManager;
 AccerionSensor* mySensor = NULL;
 void statusCb(FileSenderStatus status);
 void doneCb(bool done, bool &success);
 void progressCb(int progress);
 
};

