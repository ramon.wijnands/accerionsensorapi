#include <AccerionSensorAPI/AccerionSensorAPI.h>

class toggleRecModeManager
{
 public:
 toggleRecModeManager(Address sensorIP);
 ~toggleRecModeManager();
 bool getMap();
 void toggleRecordingMode(bool on);
 bool toggle;
 
 private:
 AccerionSensorManager* sensorManager;
 AccerionSensor* mySensor = NULL;
 void statusCb(FileSenderStatus status);
 void doneCb(bool done, bool &success);
 void progressCb(int progress);
 void recModeCB(Acknowledgement ack);
 
};

