#include "toggleRecMode.h"

toggleRecModeManager::toggleRecModeManager(Address sensorIP)
{
    sensorManager = AccerionSensorManager::getInstance();
    Address localIP;
    
    while(!mySensor)
    {
        std::cout << "Looking for sensor.." << std::endl;
        mySensor = sensorManager->getAccerionSensorByIP(sensorIP, localIP, ConnectionType::CONNECTION_TCP);
    }
    std::cout << "Found sensor!" << std::endl;

}

void toggleRecModeManager::toggleRecordingMode(bool on)
{
    auto callback = std::bind(&toggleRecModeManager::recModeCB, this, std::placeholders::_1);
    mySensor->toggleRecordingMode(on, callback);
}

void toggleRecModeManager::recModeCB(Acknowledgement ack)
{
    if (ack.value)
    {
        std::cout << "rec mode on" << std::endl;
    }
    else
    {
        std::cout << "rec mode off" << std::endl;
    }
}