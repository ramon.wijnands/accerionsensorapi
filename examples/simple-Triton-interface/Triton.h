#ifndef TRITON_H
#define TRITON_H

#include <AccerionSensorAPI/AccerionSensorAPI.h>

enum SendMapStrategy : uint8_t {
    REPLACEMAP = 0x00,
    MERGEMAP = 0x01
};

class Triton {

public:

    Triton(Address ipAddress, bool debug=false);

    void bindDriftCorrectionsCb();;

    int toggleMapping(bool on, uint16_t clusterID);

    int clearClusterLibrary();

    int setSensorPose(Pose poseStruct);

    int setMountPose(Pose poseStruct);

    int toggleAbsoluteMode(bool on);

    bool sendMap(std::string sourcePath, SendMapStrategy strategy);

protected:

    AccerionSensor* sensor_;

    uint16_t currentClusterID_;
private:

    AccerionSensorManager* sensorManager_;
    bool debug_;

    virtual void processDriftCorrection(Pose pose) = 0;

    void statusCb(FileSenderStatus status);

    void doneCb(bool done, bool &success);

    void progressCb(int progress);

    void driftCorrectionsCb(DriftCorrection driftCorrection);

};

#endif