# Script to retrieve the map from sensor (Triton, Jupiter)

Follow the steps below: 

<code> $ cd toggle-recording-mode </code> <br>
<code> $ mkdir build </code> <br>
<code> $ cd build </code> <br>
<code> $ cmake .. </code> <br>
<code> $ make </code> <br>

Now execute the command below attached by your sensor ip address and command to start recording mode:
<code> $ ./toggleRecMode 192.168.178.100 1 </code> <br>

Now execute the command below attached by your sensor ip address and command to stop recording mode:
<code> $ ./toggleRecMode 192.168.178.100 0 </code> <br>
