#include "sendMap.h"

sendMapManager::sendMapManager(Address sensorIP, std::string folderLocation, int strategy)
{
    sensorManager = AccerionSensorManager::getInstance();
    folderLocation_ = folderLocation;
    Address localIP;
    strategy_ = strategy;

    while(!mySensor)
    {
        std::cout << "Looking for sensor.." << std::endl;
        mySensor = sensorManager->getAccerionSensorByIP(sensorIP, localIP, ConnectionType::CONNECTION_TCP);
    }
    std::cout << "Found sensor!" << std::endl;

}

// std::string sendMapManager::sendMapFileName()
// {
//     SerialNumber serial = mySensor->getSerialNumberBlocking();
//     std::stringstream filename;
//     filename << folderLocation_;
//     filename << std::to_string(serial.serialNumber);

//     std::time_t ttime = std::time(0);
//     const std::tm *local_time = std::localtime(&ttime);
//     filename << "-" << std::put_time(local_time, "%d-%m-%Y-%H-%M") << ".amf";
//     return filename.str();
// }

bool sendMapManager::sendMap() {
    //std::string sourcePath = sendMapFileName();
    std::string sourcePath = folderLocation_;
    std::cout << "Trying to send map: " << sourcePath << std::endl;
    std::cout << "Strategy: " << strategy_ << std::endl;

    bool success = false;
    
    auto progress_callback = std::bind(&sendMapManager::progressCb, this, std::placeholders::_1);
    auto done_callback = std::bind(&sendMapManager::doneCb, this, std::placeholders::_1, std::ref(success));
    auto status_callback = std::bind(&sendMapManager::statusCb, this, std::placeholders::_1);
    // int strategy = 0; // strategy int value 0 = replace 1 = merge

    mySensor->sendMap(sourcePath, progress_callback, done_callback, status_callback, strategy_);

    while(!success) {
        sleep(1);
    }
    return success;
}

void sendMapManager::progressCb(int progress) {
    std::cout << "Progress: " << progress << std::endl;
}

void sendMapManager::doneCb(bool done, bool &success) {
    if (done) {
        success = true;
        std::cout << "Map transfer successful" << std::endl;
    }
    else {
        success = false;
        std::cout << "Map transfer unsuccessful" << std::endl;
    }
}

void sendMapManager::statusCb(FileSenderStatus status) {
    std::cout << "FileSenderStatus: " << status << std::endl;
}