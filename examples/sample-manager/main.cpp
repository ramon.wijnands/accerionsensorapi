#include <iostream>
#include "sampleManager.h"

int main(){
    SampleManager *mngr = new SampleManager(10,42,39,143);
    sleep(2);

    mngr->clearClusterLibrary();
    sleep(5);

    Pose startPose;
    startPose.x = 1;
    startPose.y = 2;
    startPose.heading = 3;
    mngr->setSensorPose(startPose);
    sleep(2);

    mngr->toggleMapping(true, 123);
    sleep(5);

    mngr->toggleMapping(false, 1);
    sleep(2);

    mngr->toggleAbsoluteMode(true);
    sleep(2);

    return 0;
}