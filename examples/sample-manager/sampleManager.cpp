#include <sampleManager.h>

SampleManager::SampleManager(int first, int second, int third, int fourth)
{
    sensorManager = AccerionSensorManager::getInstance();
    Address sensorIP, localIP;
    sensorIP.first = first;
    sensorIP.second = second;
    sensorIP.third = third;
    sensorIP.fourth = fourth;
    mySensor = sensorManager->getAccerionSensorByIPBlocking(sensorIP, localIP, ConnectionType::CONNECTION_TCP, 10);
    if(mySensor)
    {
        std::cout << "Requested sensor found!" << std::endl;
    }
}

void SampleManager::clearCLCB(Acknowledgement ack)
{
    if (ack.value)
    {
        std::cout << "" << std::endl;
    }
    else
    {
        std::cout << "Failed" << std::endl;
    }
}

void SampleManager::mappingCB(Acknowledgement ack)
{
    if (ack.value)
    {
        std::cout << "Mapping started, with ID:" << currentClusterID_ << std::endl;
    }
    else
    {
        std::cout << "Mapping stopped..." << std::endl;
    }
}

void SampleManager::poseCB(Pose sp)
{
    if (sp.x == pose_.x && sp.y == pose_.y && sp.heading == pose_.heading)
    {
        std::cout << "Pose set to x:" << pose_.x << ", y:" << pose_.y << ", heading:" << pose_.heading << std::endl;
    }
    else
    {
        std::cout << "Failed" << std::endl;
    }
}

void SampleManager::absCB(Acknowledgement ack)
{
    if (ack.value)
    {
        std::cout << "abs mode on" << std::endl;
    }
    else
    {
        std::cout << "abs mode off" << std::endl;
    }
}

void SampleManager::clearClusterLibrary()
{
    auto callback = std::bind(&SampleManager::clearCLCB, this, std::placeholders::_1);
    mySensor->clearClusterLibrary(callback);
}

void SampleManager::toggleMapping(bool on, uint16_t clusterID)
{
    auto callback = std::bind(&SampleManager::mappingCB, this, std::placeholders::_1);
    currentClusterID_ = clusterID;
    mySensor->toggleMapping(on, clusterID, callback);
}

void SampleManager::setSensorPose(Pose poseStruct)
{
    auto callback = std::bind(&SampleManager::poseCB, this, std::placeholders::_1);
    pose_ = poseStruct;
    mySensor->setSensorPose(poseStruct, callback);
}

void SampleManager::toggleAbsoluteMode(bool on)
{
    auto callback = std::bind(&SampleManager::absCB, this, std::placeholders::_1);
    mySensor->toggleAbsoluteMode(on, callback);
}
