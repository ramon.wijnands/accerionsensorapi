#include <Jupiter.h>
#include <TritonInterface.h>

class AbsLocalization : public Jupiter, TritonInterface {

public:

    AbsLocalization(Address jupiterIp, Address tritonIp, bool debug=false):
            Jupiter(jupiterIp, debug), TritonInterface(tritonIp, debug) {

        Jupiter::bindUncorrectedPoseCb();
        Jupiter::setMountPose(Pose{0,0,0});
        Jupiter::setSensorPose(Pose{0,0,0});
        Jupiter::toggleAbsoluteMode(false);

        Triton::bindDriftCorrectionsCb();
        Triton::setMountPose(Pose{0,0,0});
        Triton::setSensorPose(Pose{0,0,0});
        Triton::toggleAbsoluteMode(true);
    }

    void sendPoseAndCovariance(Pose pose, StandardDeviation stdev, uint64_t timeOffsetInUsec) override {

        InputPose inputPose;
        inputPose.pose = pose;
        inputPose.standardDeviation = stdev;
        inputPose.timeOffsetInUsec = timeOffsetInUsec;

        Triton::sensor_->setPoseAndCovariance(inputPose);
    }

    void processDriftCorrection(Pose poseStruct) override {

        Jupiter::sensor_->setSensorPose(poseStruct, nullptr);
    }
};

Address parseIp(std::stringstream ipArg) {

    std::string substr;
    std::vector<uint8_t> ip;

    while (getline(ipArg, substr, '.')) {
        ip.push_back(std::stoi(substr));
    }

    return Address {ip[0], ip[1], ip[2], ip[3]};

}

int main(int argc, char** argv) {

    if(argc!=3){
        std::printf("Please include Jupiter and Triton Ip addresses; e.g. ./... 10.42.0.116 10.42.0.143 \n");
        return 1;
    }

    Address jupiterIp = parseIp(std::stringstream(argv[2]));
    Address tritonIp = parseIp(std::stringstream(argv[1]));

    // Try to connect Jupiter and Triton sensors
    try {
        AbsLocalization accerionSystem(jupiterIp, tritonIp, true);
    }
    catch (const std::exception& ex ) {
        std::printf("Exception: %s \n", ex.what());
        return 0;
    }

    std::cout << "Finish absolute localization by pressing 'enter' key..." << std::endl;

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    do {
    } while(std::cin.get()!='\n');

}