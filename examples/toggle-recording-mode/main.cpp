#include <iostream>
#include "toggleRecMode.h"
#include <sstream>
#include <iomanip>
#include <string>

Address parseIp(std::stringstream ipArg) {

    std::string substr;
    std::vector<uint8_t> ip;

    while (getline(ipArg, substr, '.')) {
        ip.push_back(std::stoi(substr));
    }

    return Address {ip[0], ip[1], ip[2], ip[3]};

}

int main(int argc, char *argv[]){
    if(argc!=3){
        std::printf("Please include sensor Ip address and 1 or 0 to start or stop; e.g. ./... 10.42.0.116 1, if you want to start the recording \n");
        return 1;
    }
    Address sensorIp = parseIp(std::stringstream(argv[1]));
    int toggle_int = atoi(argv[2]);
    bool toggle = bool(toggle_int);
    toggleRecModeManager *mngr = new toggleRecModeManager(sensorIp);
    sleep(2);
    
    mngr->toggleRecordingMode(toggle);
    sleep(2);
        
    return 0;
}