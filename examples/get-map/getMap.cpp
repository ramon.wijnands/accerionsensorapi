#include "getMap.h"

getMapManager::getMapManager(Address sensorIP, std::string folderLocation)
{
    sensorManager = AccerionSensorManager::getInstance();
    folderLocation_ = folderLocation;
    Address localIP;
    
    while(!mySensor)
    {
        std::cout << "Looking for updateService.." << std::endl;
        mySensor = sensorManager->getAccerionSensorByIP(sensorIP, localIP, ConnectionType::CONNECTION_TCP);
    }
    std::cout << "Found sensor!" << std::endl;

}

std::string getMapManager::getMapFileName()
{
    SerialNumber serial = mySensor->getSerialNumberBlocking();
    std::stringstream filename;
    filename << folderLocation_;
    filename << std::to_string(serial.serialNumber);

    std::time_t ttime = std::time(0);
    const std::tm *local_time = std::localtime(&ttime);
    filename << "-" << std::put_time(local_time, "%d-%m-%Y-%H-%M") << ".amf";
    return filename.str();
}

bool getMapManager::getMap() {
    std::string destinationPath = getMapFileName();
    std::cout << "Trying to retrieve map: " << destinationPath << std::endl;

    bool success = false;
    
    auto progress_callback = std::bind(&getMapManager::progressCb, this, std::placeholders::_1);
    auto done_callback = std::bind(&getMapManager::doneCb, this, std::placeholders::_1, std::ref(success));
    auto status_callback = std::bind(&getMapManager::statusCb, this, std::placeholders::_1);

    mySensor->getMap(destinationPath, progress_callback, done_callback, status_callback);

    while(!success) {
        sleep(1);
    }
    return success;
}

void getMapManager::progressCb(int progress) {
    std::cout << "Progress: " << progress << std::endl;
}

void getMapManager::doneCb(bool done, bool &success) {
    if (done) {
        success = true;
        std::cout << "Map transfer successful" << std::endl;
    }
    else {
        success = false;
        std::cout << "Map transfer unsuccessful" << std::endl;
    }
}

void getMapManager::statusCb(FileSenderStatus status) {
    std::cout << "FileSenderStatus: " << status << std::endl;
}