#!/usr/bin/env python3
"""
 Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.'''
"""

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pandas as pd
import argumentParser

inputFile = argumentParser.args["input"] + "/floor_map_coordinates.csv"

df = pd.read_csv(inputFile)

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=[14, 6])
fig.suptitle("Map visualization")

ax.set_xlabel("X-axis (m)")
ax.set_ylabel("Y-axis (m)")

clusterIds_distinct = df["clusterID"].unique()
colors = iter(cm.rainbow(np.linspace(0, 1, len(clusterIds_distinct))))

for clusterId in clusterIds_distinct:
    df_ = df[df["clusterID"] == clusterId]
    xPos = df_["markerPosX"]
    yPos = df_["markerPosY"]

    ax.scatter(xPos, yPos, color=next(colors), marker="s")

fig.legend(labels=["clusterId " + str(clusterId) for clusterId in clusterIds_distinct],
           loc="upper right")

plt.grid()
plt.show()
