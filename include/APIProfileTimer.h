/* Copyright (C) 2019 Accerion / Unconstrained Robotics B.V. - All Rights Reserved
 * You( excluding Accerion employees) may NOT use, distribute and modify this code 
 * under any terms or the terms of any license, unless with full prior permission by 
 * Accerion supported by a contract signed by both parties.
 *
 * Accerion employees may use, distribute (only in compiled and encrypted form) and 
 * modify this code as long as it is intended for Accerion usage and in no way creates
 * a situation that in any way could affect Accerion negatively. Contact your supervisor
 * in case of doubt.
 *
 * For any questions contact Accerion
 * Author(s):
 *	-Accerion
 */
#ifndef APIPROFILETIMER_H_
#define APIPROFILETIMER_H_

#include <vector>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <chrono>
#include <utility>

#define MAX_N_SECTIONS 20  //!< Maximum number of sections to be profiled

/** 
 * @brief   Class for usage of timers for profiling purposes; it is assumed that at every loop, same number of parts will be profiled with the same order
 */
class APIProfileTimer
{
public:
    APIProfileTimer(const std::string& mainName, bool keepHistogram = false);

    ~APIProfileTimer();

    void startLoopTime();
    void endLoopTime();

    float computeCurrentThroughput();

    long getTotalLoopTime(){return totalLoopTime_;};
private:
    unsigned int idxCurrPart_     = 0;
    unsigned int nPartsToProfile_ = 0;

    std::chrono::high_resolution_clock::time_point currTime_, prevTime_, loopStartTime_, loopEndTime_;

    long        totalLoopTime_;

    std::string mainName_;
    long        loopCount_;
    float       currThroughput_;


    int             histGridSz_;
    unsigned int    histSize_;
    bool            keepHistogram_;
    std::vector<unsigned int> histogramStepTimes_;
};


#endif
