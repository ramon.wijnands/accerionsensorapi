#include <Jupiter.h>

Jupiter::Jupiter(Address jupiterIp, bool debug) : debug_(debug) {

    jupiterManager_ = AccerionSensorManager::getInstance();

    Address localIp;
    std::cout << "Connecting Jupiter" << std::endl;

        sensor_ = jupiterManager_->getAccerionSensorByIPBlocking(
                jupiterIp, localIp, ConnectionType::CONNECTION_TCP, 10);

        if (sensor_){
            std::printf("Requested sensor_ found \n");
        }
        else {
            throw std::runtime_error("Can't connect to sensor");
        }
}

void Jupiter::bindUncorrectedPoseCb() {

    auto callback = std::bind(&Jupiter::uncorrectedPoseCb, this, std::placeholders::_1);

    sensor_->subscribeToUncorrectedPose(callback);
}

void Jupiter::uncorrectedPoseCb(UncorrectedPose uncorrectedPose) {
    Pose pose = uncorrectedPose.pose;
    StandardDeviation stdev {0.1, 0.1, 0.1};
    uint64_t timeOffsetInUsec;

    if (debug_) {
        std::printf("(Relative pose) x: %f, y: %f, heading: %f \n", pose.x, pose.y, pose.heading);
    }

    sendPoseAndCovariance(pose, stdev, timeOffsetInUsec);
}

int Jupiter::toggleAbsoluteMode(bool on) {
    return sensor_->toggleAbsoluteModeBlocking(on);
}

int Jupiter::setMountPose(Pose poseStruct) {
    return sensor_->setSensorMountPoseBlocking(poseStruct);
}

int Jupiter::setSensorPose(Pose poseStruct) {
    return sensor_->setSensorPoseBlocking(poseStruct);
}