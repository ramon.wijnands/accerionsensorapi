#ifndef EXTERNAL_REFERENCE_INTERFACE_H
#define EXTERNAL_REFERENCE_INTERFACE_H

#include <AccerionSensorAPI/AccerionSensorAPI.h>
#include <Triton.h>

class TritonInterface: public Triton {

public:

    TritonInterface(Address ipAddress, bool debug): Triton(ipAddress, debug){}

private:

    virtual void processDriftCorrection(Pose pose) = 0;

    virtual void sendPoseAndCovariance(Pose pose, StandardDeviation stdev, u_int64_t timeOffsetInUsec) = 0;

};

#endif