/* Copyright (C) 2019 Accerion / Unconstrained Robotics B.V. - All Rights Reserved
 * You( excluding Accerion employees) may NOT use, distribute and modify this code 
 * under any terms or the terms of any license, unless with full prior permission by 
 * Accerion supported by a contract signed by both parties.
 *
 * Accerion employees may use, distribute (only in compiled and encrypted form) and 
 * modify this code as long as it is intended for Accerion usage and in no way creates
 * a situation that in any way could affect Accerion negatively. Contact your supervisor
 * in case of doubt.
 *
 * For any questions contact Accerion
 * Author(s):
 *	-Accerion
 */
#include "APISysUtils.h"

using namespace std;

bool APISysUtils::removeFile(const string &fileToRemove)
{
    if( remove( fileToRemove.c_str() ) != 0 )
    {
        std::cout << "Error deleting file: " << fileToRemove << std::endl;
        return false;
    }    
    else
    {
        std::cout << "File successfully deleted: " << fileToRemove << std::endl;
        return true;
    }
}

bool APISysUtils::fileExists(const std::string& filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}
