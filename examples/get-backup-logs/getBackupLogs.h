#include <AccerionSensorAPI/AccerionSensorAPI.h>

class getBackupLogsManager
{
 public:
 getBackupLogsManager(Address sensorIP, std::string folderLocation);
 ~getBackupLogsManager();
 bool getBackupLogs();
 std::string getBackupLogFileName();
 std::string folderLocation_;
 
 private:
 AccerionUpdateServiceManager* updateManager;
 AccerionUpdateService* myUS = NULL;
 void statusCb(FileSenderStatus status);
 void doneCb(bool done, bool &success);
 void progressCb(int progress);
 
};

