# Script to send a map to sensor (Triton, Jupiter)

**Prerequisites** <br>
Build the API first.

**Build send-map example** <br>
Follow the steps below: 

<code> $ cd send-map </code> <br>
<code> $ mkdir build </code> <br>
<code> $ cd build </code> <br>
<code> $ cmake .. </code> <br>
<code> $ make </code> <br>

**Syntax** <br>
Now execute the commmand below attached by your sensor ip address, full path to .amf file and strategy (0:replace, 1:merge). Example to merge a map:

<code> $ ./sendMap 192.168.0.10 /home/$USER/598100136-28-09-2020-14-28.amf 1 </code> <br>

**Example** <br>
<code> demykremers@demykremers-HP-ProBook-450-G6:~/codes/accerionsensorapi/examples/send-map/build $ ./sendMap 192.168.0.10 /home/demykremers/codes/accerionsensorapi/examples/get-map/build/598100173-05-11-2020-13-31.amf 0 </code> <br>

**Response** <br>
Looking for sensor.. <br> 
Sensor with serial number found: 598100173 <br> 
Sending commands size: 0 <br> 
Found sensor! <br> 
Trying to send map: /home/demykremers/codes/accerionsensorapi/examples/get-map/build/598100173-05-11-2020-13-31.amf <br> 
Strategy: 0 <br> 
FileSenderStatus: 2 <br> 
Map piece sent.. <br> 
FileSenderStatus: 2 <br> 
Progress: 100 <br> 
FileSenderStatus: 4 <br> 
FileSenderStatus: 4 <br> 
Map transfer successful <br>

**Result** <br>
598100173-05-11-2020-13-31.amf map now loaded onto sensor 