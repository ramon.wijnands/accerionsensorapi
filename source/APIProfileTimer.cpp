/* Copyright (C) 2019 Accerion / Unconstrained Robotics B.V. - All Rights Reserved
 * You( excluding Accerion employees) may NOT use, distribute and modify this code 
 * under any terms or the terms of any license, unless with full prior permission by 
 * Accerion supported by a contract signed by both parties.
 *
 * Accerion employees may use, distribute (only in compiled and encrypted form) and 
 * modify this code as long as it is intended for Accerion usage and in no way creates
 * a situation that in any way could affect Accerion negatively. Contact your supervisor
 * in case of doubt.
 *
 * For any questions contact Accerion
 * Author(s):
 *	-Accerion
 */
#include "APIProfileTimer.h"

APIProfileTimer::APIProfileTimer(const std::string& mainName, bool keepHistogram)
{
    mainName_          = mainName;
    nPartsToProfile_   = 0;
    idxCurrPart_       = 0;
    loopCount_         = 0;
    currThroughput_    = 0;
    keepHistogram_     = keepHistogram;
    histGridSz_        = 10;
    histSize_          = 300/histGridSz_;
    
    histogramStepTimes_.resize(histSize_);
    std::fill(histogramStepTimes_.begin(), histogramStepTimes_.end(), 0);

}

APIProfileTimer::~APIProfileTimer()
{

}

void APIProfileTimer::startLoopTime()
{
    nPartsToProfile_ = idxCurrPart_;
    idxCurrPart_     = 0;

    loopStartTime_  = std::chrono::high_resolution_clock::now();
    currTime_   = loopStartTime_;
    prevTime_   = currTime_;
}

void APIProfileTimer::endLoopTime()
{
    loopEndTime_ = std::chrono::high_resolution_clock::now();
    loopCount_++;

    float currThroughput   = computeCurrentThroughput();

    if(keepHistogram_)
    {
        // float currThroughput   = computeCurrentThroughput();
        unsigned int histEntry = (int)(currThroughput/histGridSz_);
        if( histEntry < histSize_ )
        {
            histogramStepTimes_[histEntry]++;
        }
        else
        {
            histogramStepTimes_[histSize_-1]++;
        }
    }
    totalLoopTime_  = std::chrono::duration_cast<std::chrono::microseconds>( loopEndTime_ - loopStartTime_ ).count();
}

float APIProfileTimer::computeCurrentThroughput()
{   
    currThroughput_ = 1000000.0/((float)totalLoopTime_);
    return currThroughput_;
}