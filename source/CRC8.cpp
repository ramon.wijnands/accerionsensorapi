/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "CRC8.h"

crc crcTable[256];

CRC8::CRC8()
{
}

CRC8::~CRC8()
{
}

void CRC8::crcInit()
{
	crc  remainder;

	/*
	* Compute the remainder of each possible dividend.
	*/
	for (int dividend = 0; dividend < 256; ++dividend)
	{
		/*
		* Start with the dividend followed by zeros.
		*/
		remainder = dividend << (WIDTH - 8);

		/*
		* Perform modulo-2 division, a bit at a time.
		*/
		for (uint8_t bit = 8; bit > 0; --bit)
		{
			/*
			* Try to divide the current data bit.
			*/
			if (remainder & TOPBIT)
			{
				remainder = (remainder << 1) ^ POLYNOMIAL;
			}
			else
			{
				remainder = (remainder << 1);
			}
		}

		/*
		* Store the result into the table.
		*/
		crcTable[dividend] = remainder;
	}
}

crc CRC8::crcFast(uint8_t const message[], int nBytes)
{
	//uint8_t data;
	crc remainder = 0;

	/*
	* Divide the message by the polynomial, a byte at a time.
	*/
	for (int byte = 0; byte < nBytes; ++byte)
	{
		uint8_t data = message[byte] ^ (remainder >> (WIDTH - 8));
		remainder = crcTable[data] ^ (remainder << 8);
	}

	/*
	* The final remainder is the CRC.
	*/
	return (remainder);
}
