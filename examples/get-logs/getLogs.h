#include <AccerionSensorAPI/AccerionSensorAPI.h>

class getLogsManager
{
 public:
 getLogsManager(Address sensorIP, std::string folderLocation);
 ~getLogsManager();
 bool getLogs();
 std::string getLogFileName();
 std::string folderLocation_;
 
 private:
 AccerionUpdateServiceManager* updateManager;
 AccerionUpdateService* myUS = NULL;
 void statusCb(FileSenderStatus status);
 void doneCb(bool done, bool &success);
 void progressCb(int progress);
 
};

