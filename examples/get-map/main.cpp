#include <iostream>
#include "getMap.h"
#include <sstream>
#include <iomanip>
#include <string>

Address parseIp(std::stringstream ipArg) {

    std::string substr;
    std::vector<uint8_t> ip;

    while (getline(ipArg, substr, '.')) {
        ip.push_back(std::stoi(substr));
    }

    return Address {ip[0], ip[1], ip[2], ip[3]};

}

int main(int argc, char *argv[]){
    if(argc!=3){
        std::printf("Please include sensor Ip address and folder location; e.g. ./... 10.42.0.116 /home/pi/ \n");
        return 1;
    }
    Address sensorIp = parseIp(std::stringstream(argv[1]));
    std::string folderLocation = std::string(argv[2]);
    getMapManager *mngr = new getMapManager(sensorIp, folderLocation);
    sleep(2);
    
    mngr->getMap();
    sleep(2);
        
    return 0;
}