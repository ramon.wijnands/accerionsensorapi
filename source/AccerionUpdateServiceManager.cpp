/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionUpdateServiceManager.h"

void AccerionUpdateServiceManager::runUDPCommunication()
{
    APIProfileTimer profileTimer("AccerionUpdateServiceManager UDP thread", true);
    float maxDuration = 1e6/NetworkConstants::APIThroughput;
    auto * udpReceiver = new UDPReceiver(UPDATE_SERVICE_TRANSMIT_PORT_UDP);
    std::vector<Command> incomingCommandsTotal_;
    std::vector<uint8_t> receivedMSG_;
    std::cout << "[AccerionUpdateServiceManager] - Started Listening For Heartbeat Messages" << std::endl;

    while(true)
    {
        profileTimer.startLoopTime();
        //receive
        while(udpReceiver->ReceiveMessage())
        {
            receivedMSG_.clear();
            receivedMSG_.insert(receivedMSG_.end(), udpReceiver->getReceivedMessage(), udpReceiver->getReceivedMessage() + udpReceiver->getReceivedNumOfBytes());
            receivedCommand_.clear();
            parseMessage(incomingCommandsTotal_, receivedMSG_);
        }
        incomingCommandsTotal_.clear();

        profileTimer.endLoopTime();
        long totalTime = profileTimer.getTotalLoopTime();
        if(totalTime < maxDuration)
        {
            int delay = maxDuration - totalTime;
            if(delay > 0)
            {
                std::this_thread::sleep_for(std::chrono::microseconds(delay));
            }
        }
    }
    std::cout << "[AccerionUpdateServiceManager] - Stopped Listening For Heartbeat Messages" << std::endl;
}

AccerionUpdateServiceManager* AccerionUpdateServiceManager::getInstance()
{
    static AccerionUpdateServiceManager instance;

    return &instance;
}

AccerionUpdateServiceManager::AccerionUpdateServiceManager()
{
     /*Initialize CRC Checking*/
    crc8_.crcInit();
    //launch communication on different thread..
    std::thread t(&AccerionUpdateServiceManager::runUDPCommunication, this);
    t.detach();
}

std::vector<SensorDetails> AccerionUpdateServiceManager::getAllUpdateServices()
{
    std::vector<SensorDetails> returnList;
    std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
    for(auto scd: updateServices_)
    {
        if(std::chrono::duration_cast<std::chrono::seconds>(now - scd.lastMessageTimePoint).count() < 10)
        {
            SensorDetails sd;
            sd.ipAddress = scd.ipAddress;
            sd.serialNumber = scd.serialNumber;
            returnList.push_back(sd);
        }
    }
    return returnList;
}

AccerionUpdateService* AccerionUpdateServiceManager::getAccerionUpdateServiceByIP(Address ip, Address localIP)
{
    std::string serial;
    for(auto scd: updateServices_)
    {
        if(scd.ipAddress.first == ip.first && scd.ipAddress.second == ip.second && scd.ipAddress.third == ip.third && scd.ipAddress.fourth == ip.fourth)
        {
            serial = scd.serialNumber;
        }
    }
    if(!serial.empty())
    {
        std::cout << "UpdateService with serial number found: " << serial << std::endl;
        //check if exists
        for(auto p : createdUpdateServices_)
        {
           if(p.first.first.first == ip.first && p.first.first.second == ip.second && p.first.first.third == ip.third && p.first.first.fourth == ip.fourth)
           {
               std::cout << "UpdateService was already created" << std::endl;
               return p.second;
           }
        }
        // no so create
        std::cout << "Creating new UpdateService object.." << std::endl;
        AccerionUpdateService * newUpdateService = new AccerionUpdateService(ip, serial, localIP);
        std::pair<Address, std::string> innerPair = std::make_pair<>(ip, serial);
        std::pair<std::pair<Address, std::string>, AccerionUpdateService*> outerPair = std::make_pair<>(innerPair, newUpdateService);
        createdUpdateServices_.push_back(outerPair);
        return newUpdateService;
    }
    std::cout << "Sensor not found" << std::endl;
    return nullptr;
}

AccerionUpdateService* AccerionUpdateServiceManager::getAccerionUpdateServiceBySerial(std::string serial, Address localIP)
{
    Address ip;
    bool found = false;
    for(auto scd: updateServices_)
    {
        if(scd.serialNumber == serial)
        {
            ip = scd.ipAddress;
            found = true;
        }
    }
    if(found)
    {
        std::cout << "UpdateService with serial number found: " << serial << std::endl;
        //check if exists
        for(auto p : createdUpdateServices_)
        {
            if(p.first.second == serial)
            {
                std::cout << "UpdateService was already created" << std::endl;
                return p.second;
            }
        }
        // no so create
        std::cout << "Creating new UpdateService object.." << std::endl;
        AccerionUpdateService * newUpdateService = new AccerionUpdateService(ip, serial, localIP);
        std::pair<Address, std::string> innerPair = std::make_pair<>(ip, serial);
        std::pair<std::pair<Address, std::string>, AccerionUpdateService*> outerPair = std::make_pair<>(innerPair, newUpdateService);
        createdUpdateServices_.push_back(outerPair);
        return newUpdateService;
    }
    std::cout << "Sensor not found" << std::endl;
    return nullptr;
}

void AccerionUpdateServiceManager::parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_)
{
    uint8_t receivedSerialNumberData[4];

    //uint8_t receivedTotalBytesInMessageData[4];
    uint32_t receivedTotalBytesInMessage;
    uint32_t numberOfBytesLeft = receivedMessage_.size();
    uint32_t numberOfReadBytes = 0;

    while (numberOfBytesLeft > 0)
    {
        receivedCommand_.clear();
        
        if (numberOfBytesLeft >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
        {
            for (unsigned int i = 0; i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i++)
            {
                /*Get Serial Number*/
                if (i >= CommandFields::SERIAL_NUMBER_BEGIN_BYTE && i < CommandFields::SERIAL_NUMBER_BEGIN_BYTE + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH)
                {
                    receivedSerialNumberData[i] = receivedMessage_[i + numberOfReadBytes];
                }
                /*Get Command Number*/
                else if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE && i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
                {
                    receivedCommandID_ = receivedMessage_[i + numberOfReadBytes];
                    if(debugMode_)
                        std::cout << "[AccerionUpdateServiceManager::parseMessages] Received command: " << std::hex << +receivedCommandID_ << std::dec << std::endl;
                }
            }
            // Extract number of bytes in message
            	if(receivedCommandID_ != CMD_REPLACE_CLUSTER_G2O && receivedCommandID_ != CMD_PLACE_MAP && receivedCommandID_ != ACK_CONSOLE_OUTPUT_INFO){
		        auto iter = commandValues.find(receivedCommandID_);
		        if(iter != commandValues.end())
		        {
		            receivedTotalBytesInMessage  = std::get<1>(iter->second);//std::get<1>(commandValues.at(receivedCommandID_));
		        }
		        else
		        {
		            std::cout << "Unknown Command received" << std::endl;
		            receivedTotalBytesInMessage = 0;
		        }
		}else{
                uint8_t receivedNumOfBytesData[4];
                receivedNumOfBytesData[0] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes];
                receivedNumOfBytesData[1] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 1];
                receivedNumOfBytesData[2] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 2];
                receivedNumOfBytesData[3] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 3];

                receivedTotalBytesInMessage = ntohl(*((uint32_t*)&receivedNumOfBytesData));
                if(debugMode_)
                    std::cout << "[AccerionUpdateServiceManager::parseMessages] Received length: " << receivedTotalBytesInMessage << std::endl;
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }

        if (receivedTotalBytesInMessage <= numberOfBytesLeft)
        {
            for (unsigned int i = CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i < receivedTotalBytesInMessage; i++)
            {
                /*Get Command Data*/
                if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH && i < receivedTotalBytesInMessage - 1)
                {
                    receivedCommand_.push_back(receivedMessage_[i + numberOfReadBytes]);
                }
                /*Get CRC8 Data*/
                else
                {
                    receivedCRC8_ = receivedMessage_[i + numberOfReadBytes];
                }
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }
        lastMessageWasBroken_ = false;

        /*Check serial number*/
        receivedSerialNumber_ = ntohl(*((uint32_t *)&receivedSerialNumberData));
        if(receivedCommandID_ == PRD_HEARTBEAT_INFO)
        {
            Address ip;
            ip.first   = receivedCommand_[0];
            ip.second  = receivedCommand_[1];
            ip.third   = receivedCommand_[2];
            ip.fourth  = receivedCommand_[3];
            std::string serial = std::to_string(receivedSerialNumber_);
            bool exists = false;
            for(auto &scd : updateServices_)
            {
                if(scd.ipAddress.first == ip.first && scd.ipAddress.second == ip.second && scd.ipAddress.third == ip.third && scd.ipAddress.fourth == ip.fourth && scd.serialNumber == serial)
                {
                    exists = true;
                    scd.lastMessageTimePoint = std::chrono::high_resolution_clock::now();
                }
            }
            if(!exists)
            {
                SensorConnectionDetails scd;
                scd.ipAddress = ip;
                scd.serialNumber = serial;
                scd.lastMessageTimePoint = std::chrono::high_resolution_clock::now();
                updateServices_.push_back(scd);
            }
        }
        uint8_t calculatedCRC8 = crc8_.crcFast((uint8_t*)receivedMessage_.data() + numberOfReadBytes, receivedTotalBytesInMessage - 1);
        if (receivedSerialNumber_ != sensorSerialNumber_ && receivedSerialNumber_ != DEFAULT_SERIAL_NUMBER && sensorSerialNumber_ != DEFAULT_SERIAL_NUMBER)
        {
            if(debugMode_)
                std::cout << "Received Message is not for this Jupiter, which has serial number := " << std::hex << sensorSerialNumber_ << " it is intended for serial number := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            messageReady_ = false;
            return;
        }
        if (receivedCRC8_ != calculatedCRC8)
        {
            if(debugMode_)
                std::cout << "Received Message does not have the correct crc8 code, it has the wrong code := " << std::hex << +receivedCRC8_ << std::dec << std::endl;
            messageReady_ = false;
            return;
        }

        commands.emplace_back(receivedCommandID_, receivedCommand_);

        numberOfReadBytes += receivedTotalBytesInMessage;
        numberOfBytesLeft -= receivedTotalBytesInMessage;
        receivedCommand_.clear();
    }
}