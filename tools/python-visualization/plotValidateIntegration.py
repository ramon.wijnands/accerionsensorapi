#!/usr/bin/env python3
"""
 Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.'''
"""

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pandas as pd
import argumentParser

inputFile = argumentParser.args["input"] + "/logRuntimeTracking.csv"

df = pd.read_csv(inputFile)
time = df['time[s]']

fig, ax = plt.subplots(nrows=4, ncols=1, figsize=[8, 8])

fig.suptitle("Drift correction check")
input_marker_size = 10
output_marker_size = 0.5

### Plot x-axis
xInput = df['inputPoseX[m]']
xOutput = df['outputPoseX[m]']

ax[0].set_xlabel("Time (sec)")
ax[0].set_ylabel("X-axis (m)")
ax[0].plot(time, xInput, marker='.', linestyle='None', markersize=input_marker_size)
ax[0].plot(time, xOutput, marker='.', linestyle='None', markersize=output_marker_size)

### Plot y-axis
yInput = df['inputPoseY[m]']
yOutput = df['outputPoseY[m]']

ax[1].set_xlabel("Time (sec)")
ax[1].set_ylabel("Y-axis (m)")
ax[1].plot(time, yInput, marker='.', linestyle='None', markersize=input_marker_size)
ax[1].plot(time, yOutput, marker='.', linestyle='None', markersize=output_marker_size)

### Plot theta
thInput = df['inputPoseTh[rad]']
thOutput = df['outputPoseTh[rad]']

ax[2].set_xlabel("Time (sec)")
ax[2].set_ylabel("Angle (rad)")
ax[2].plot(time, thInput, marker='.', linestyle='None', markersize=input_marker_size)
ax[2].plot(time, thOutput, marker='.', linestyle='None', markersize=output_marker_size)

### Ext. reference frequency
input_received_mask = pd.isna(df['inputPoseX[m]'])
throughput_mov_avg = np.cumsum(input_received_mask)/time
ax[3].set_xlabel("Time (sec)")
ax[3].set_ylabel("Ext-ref freq. (sec)")

ax[3].plot(time, throughput_mov_avg, marker='.', linestyle='None')

fig.legend(labels=["InputPose", "OutputPose"],
           loc="upper right",
           borderaxespad=1.0)

plt.subplots_adjust(hspace=.4)
plt.show()
