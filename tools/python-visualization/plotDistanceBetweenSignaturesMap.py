#!/usr/bin/env python3
"""
 Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.'''
"""

from argumentParser import *
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator,
                               FormatStrFormatter,
                               AutoMinorLocator)
import numpy as np
import pandas as pd
import argumentParser

# Parse arguments
inputFile = argumentParser.args["input"] + "/floor_map_coordinates.csv"
df = pd.read_csv(inputFile)

clusterIds_distinct = df["clusterID"].unique()
markerX, markerY, dist = [], [], []

for clusterId in clusterIds_distinct:
    df_ = df[df["clusterID"] == clusterId]
    df_ = df_.sort_values(by=["markerID"])

    markerX_ = df_["markerPosX"].to_list()
    markerY_ = df_["markerPosY"].to_list()

    array = np.concatenate((markerX_, markerY_))

    # Euclidean distance between consecutive markers
    dist_ = np.sqrt(
        np.diff(markerX_, prepend=markerX_[0]) ** 2 + np.diff(markerY_, prepend=markerY_[0]) ** 2)

    markerX.extend(markerX_)
    markerY.extend(markerY_)
    dist.extend(dist_)

markerX, markerY, dist = np.array(markerX), np.array(markerY), np.array(dist) #Convert to np.array

plt.title("Map visualization")
plt.xlabel("X-axis (m)"); plt.ylabel("Y-axis (m)")

plt.scatter(markerX, markerY, c=dist, s=dist * 20)

cbar = plt.colorbar(ticks=np.linspace(dist.min(), dist.max(), 10)); cbar.ax.set_title('Dist m.');
cbar.ax.yaxis.set_major_formatter(FormatStrFormatter('%.4f'))

plt.grid() ; plt.tight_layout()

plt.savefig('dist_between_signatures.png'); plt.show()
