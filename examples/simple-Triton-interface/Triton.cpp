#include <Triton.h>

Triton::Triton(Address sensorIp, bool debug) : debug_(debug) {

    sensorManager_ = AccerionSensorManager::getInstance();

    Address localIp;
    std::cout << "Connecting Triton" << std::endl;
    sensor_= sensorManager_->getAccerionSensorByIPBlocking(
            sensorIp, localIp, ConnectionType::CONNECTION_TCP, 10);

    if (sensor_) {
        std::printf("Requested sensor_ found \n");
    }
    else{
        throw std::runtime_error("Can't connect to sensor");
    }

}

void Triton::bindDriftCorrectionsCb() {

    auto callback = std::bind(&Triton::driftCorrectionsCb, this, std::placeholders::_1);

    sensor_->subscribeToDriftCorrections(callback);
}

void Triton::driftCorrectionsCb(DriftCorrection driftCorrection) {
    if (debug_) {
        std::printf("(Drift correction) x: %f, y: %f, heading: %f \n", driftCorrection.pose.x, driftCorrection.pose.y, driftCorrection.pose.heading);
    }

    processDriftCorrection(driftCorrection.pose);
}

int Triton::clearClusterLibrary() {
    return sensor_->clearClusterLibraryBlocking();
}

int Triton::toggleMapping(bool on, uint16_t clusterID) {
    return sensor_->toggleMappingBlocking(on, clusterID);
}

int Triton::toggleAbsoluteMode(bool on) {
    return sensor_->toggleAbsoluteModeBlocking(on);
}

int Triton::setMountPose(Pose poseStruct) {
    return sensor_->setSensorMountPoseBlocking(poseStruct);
}

int Triton::setSensorPose(Pose poseStruct) {
    return sensor_->setSensorPoseBlocking(poseStruct);
}

bool Triton::sendMap(std::string sourcePath, SendMapStrategy strategy) {
    std::cout << "Trying to send map: " << sourcePath << std::endl;

    bool success = false;

    std::condition_variable send_map;

    auto progress_callback = std::bind(&Triton::progressCb, this, std::placeholders::_1);
    auto done_callback = std::bind(&Triton::doneCb, this, std::placeholders::_1, std::ref(success));
    auto status_callback = std::bind(&Triton::statusCb, this, std::placeholders::_1);

    sensor_->sendMap(sourcePath, progress_callback, done_callback, status_callback, strategy);

    while(!success) {
        sleep(1);
    }
    return success;
}

void Triton::progressCb(int progress) {
    std::cout << "Progress: " << progress << std::endl;
}

void Triton::doneCb(bool done, bool &success) {
    if (done) {
        success = true;
        std::cout << "Map transfer successful" << std::endl;
    }
    else {
        success = false;
        std::cout << "Map transfer unsuccessful" << std::endl;
    }
}

void Triton::statusCb(FileSenderStatus status) {
    std::cout << "FileSenderStatus: " << status << std::endl;
}
