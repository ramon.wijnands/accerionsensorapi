#ifndef JUPITER_H
#define JUPITER_H

#include <AccerionSensorAPI/AccerionSensorAPI.h>

class Jupiter {

public:

    Jupiter(Address ipAddress, bool debug=false);

    void bindUncorrectedPoseCb();

    int setSensorPose(Pose poseStruct);

    int setMountPose(Pose poseStruct);

    int toggleAbsoluteMode(bool on);

protected:

    AccerionSensor* sensor_;

private:

    AccerionSensorManager* jupiterManager_;

    bool debug_;

    virtual void sendPoseAndCovariance(Pose pose, StandardDeviation stdev, u_int64_t timeOffsetInUsec) = 0;


    void uncorrectedPoseCb(UncorrectedPose uncorrectedPose);
};

#endif